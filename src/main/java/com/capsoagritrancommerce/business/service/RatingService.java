/*
 * Created on 8 mars 2015 ( Time 12:20:36 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.capsoagritrancommerce.business.service;

import java.util.List;

import com.capsoagritrancommerce.entity.Rating;

/**
 * Business Service Interface for entity Rating.
 */
public interface RatingService { 

	/**
	 * Loads an entity from the database using its Primary Key
	 * @param id
	 * @return entity
	 */
	Rating findById( Integer id  ) ;

	/**
	 * Loads all entities.
	 * @return all entities
	 */
	List<Rating> findAll();

	/**
	 * Saves the given entity in the database (create or update)
	 * @param entity
	 * @return entity
	 */
	Rating save(Rating entity);

	/**
	 * Updates the given entity in the database
	 * @param entity
	 * @return
	 */
	Rating update(Rating entity);

	/**
	 * Creates the given entity in the database
	 * @param entity
	 * @return
	 */
	Rating create(Rating entity);

	/**
	 * Deletes an entity using its Primary Key
	 * @param id
	 */
	void delete( Integer id );


}
