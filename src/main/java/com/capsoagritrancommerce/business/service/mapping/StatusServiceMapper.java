/*
 * Created on 8 mars 2015 ( Time 12:20:36 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.capsoagritrancommerce.business.service.mapping;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;
import com.capsoagritrancommerce.entity.Status;
import com.capsoagritrancommerce.entity.jpa.StatusEntity;

/**
 * Mapping between entity beans and display beans.
 */
@Component
public class StatusServiceMapper extends AbstractServiceMapper {

	/**
	 * ModelMapper : bean to bean mapping library.
	 */
	private ModelMapper modelMapper;
	
	/**
	 * Constructor.
	 */
	public StatusServiceMapper() {
		modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	/**
	 * Mapping from 'StatusEntity' to 'Status'
	 * @param statusEntity
	 */
	public Status mapStatusEntityToStatus(StatusEntity statusEntity) {
		if(statusEntity == null) {
			return null;
		}

		//--- Generic mapping 
		Status status = map(statusEntity, Status.class);

		return status;
	}
	
	/**
	 * Mapping from 'Status' to 'StatusEntity'
	 * @param status
	 * @param statusEntity
	 */
	public void mapStatusToStatusEntity(Status status, StatusEntity statusEntity) {
		if(status == null) {
			return;
		}

		//--- Generic mapping 
		map(status, statusEntity);

	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ModelMapper getModelMapper() {
		return modelMapper;
	}

	protected void setModelMapper(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

}