/*
 * Created on 8 mars 2015 ( Time 12:20:36 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.capsoagritrancommerce.business.service.mapping;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;
import com.capsoagritrancommerce.entity.Musictype;
import com.capsoagritrancommerce.entity.jpa.MusictypeEntity;

/**
 * Mapping between entity beans and display beans.
 */
@Component
public class MusictypeServiceMapper extends AbstractServiceMapper {

	/**
	 * ModelMapper : bean to bean mapping library.
	 */
	private ModelMapper modelMapper;
	
	/**
	 * Constructor.
	 */
	public MusictypeServiceMapper() {
		modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	/**
	 * Mapping from 'MusictypeEntity' to 'Musictype'
	 * @param musictypeEntity
	 */
	public Musictype mapMusictypeEntityToMusictype(MusictypeEntity musictypeEntity) {
		if(musictypeEntity == null) {
			return null;
		}

		//--- Generic mapping 
		Musictype musictype = map(musictypeEntity, Musictype.class);

		return musictype;
	}
	
	/**
	 * Mapping from 'Musictype' to 'MusictypeEntity'
	 * @param musictype
	 * @param musictypeEntity
	 */
	public void mapMusictypeToMusictypeEntity(Musictype musictype, MusictypeEntity musictypeEntity) {
		if(musictype == null) {
			return;
		}

		//--- Generic mapping 
		map(musictype, musictypeEntity);

	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ModelMapper getModelMapper() {
		return modelMapper;
	}

	protected void setModelMapper(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

}