/*
 * Created on 8 mars 2015 ( Time 12:20:36 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.capsoagritrancommerce.business.service;

import java.util.List;

import com.capsoagritrancommerce.entity.WhishlistHasProduct;

/**
 * Business Service Interface for entity WhishlistHasProduct.
 */
public interface WhishlistHasProductService { 

	/**
	 * Loads an entity from the database using its Primary Key
	 * @param whishlistId
	 * @param productId
	 * @return entity
	 */
	WhishlistHasProduct findById( Integer whishlistId, Integer productId  ) ;

	/**
	 * Loads all entities.
	 * @return all entities
	 */
	List<WhishlistHasProduct> findAll();

	/**
	 * Saves the given entity in the database (create or update)
	 * @param entity
	 * @return entity
	 */
	WhishlistHasProduct save(WhishlistHasProduct entity);

	/**
	 * Updates the given entity in the database
	 * @param entity
	 * @return
	 */
	WhishlistHasProduct update(WhishlistHasProduct entity);

	/**
	 * Creates the given entity in the database
	 * @param entity
	 * @return
	 */
	WhishlistHasProduct create(WhishlistHasProduct entity);

	/**
	 * Deletes an entity using its Primary Key
	 * @param whishlistId
	 * @param productId
	 */
	void delete( Integer whishlistId, Integer productId );


}
