/*
 * Created on 8 mars 2015 ( Time 12:20:36 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.capsoagritrancommerce.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.capsoagritrancommerce.entity.Status;
import com.capsoagritrancommerce.entity.jpa.StatusEntity;
import java.util.List;
import com.capsoagritrancommerce.business.service.StatusService;
import com.capsoagritrancommerce.business.service.mapping.StatusServiceMapper;
import com.capsoagritrancommerce.data.repository.jpa.StatusJpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of StatusService
 */
@Component
@Transactional
public class StatusServiceImpl implements StatusService {

	@Resource
	private StatusJpaRepository statusJpaRepository;

	@Resource
	private StatusServiceMapper statusServiceMapper;
	
	@Override
	public Status findById(Integer id) {
		StatusEntity statusEntity = statusJpaRepository.findOne(id);
		return statusServiceMapper.mapStatusEntityToStatus(statusEntity);
	}

	@Override
	public List<Status> findAll() {
		Iterable<StatusEntity> entities = statusJpaRepository.findAll();
		List<Status> beans = new ArrayList<Status>();
		for(StatusEntity statusEntity : entities) {
			beans.add(statusServiceMapper.mapStatusEntityToStatus(statusEntity));
		}
		return beans;
	}

	@Override
	public Status save(Status status) {
		return update(status) ;
	}

	@Override
	public Status create(Status status) {
		StatusEntity statusEntity = statusJpaRepository.findOne(status.getId());
		if( statusEntity != null ) {
			throw new IllegalStateException("already.exists");
		}
		statusEntity = new StatusEntity();
		statusServiceMapper.mapStatusToStatusEntity(status, statusEntity);
		StatusEntity statusEntitySaved = statusJpaRepository.save(statusEntity);
		return statusServiceMapper.mapStatusEntityToStatus(statusEntitySaved);
	}

	@Override
	public Status update(Status status) {
		StatusEntity statusEntity = statusJpaRepository.findOne(status.getId());
		statusServiceMapper.mapStatusToStatusEntity(status, statusEntity);
		StatusEntity statusEntitySaved = statusJpaRepository.save(statusEntity);
		return statusServiceMapper.mapStatusEntityToStatus(statusEntitySaved);
	}

	@Override
	public void delete(Integer id) {
		statusJpaRepository.delete(id);
	}

	public StatusJpaRepository getStatusJpaRepository() {
		return statusJpaRepository;
	}

	public void setStatusJpaRepository(StatusJpaRepository statusJpaRepository) {
		this.statusJpaRepository = statusJpaRepository;
	}

	public StatusServiceMapper getStatusServiceMapper() {
		return statusServiceMapper;
	}

	public void setStatusServiceMapper(StatusServiceMapper statusServiceMapper) {
		this.statusServiceMapper = statusServiceMapper;
	}

}
