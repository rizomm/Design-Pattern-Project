/*
 * Created on 8 mars 2015 ( Time 12:20:09 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.capsoagritrancommerce.rest.controller;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.capsoagritrancommerce.entity.Whishlist;
import com.capsoagritrancommerce.business.service.WhishlistService;
import com.capsoagritrancommerce.web.listitem.WhishlistListItem;

/**
 * Spring MVC controller for 'Whishlist' management.
 */
@Controller
public class WhishlistRestController {

	@Resource
	private WhishlistService whishlistService;
	
	@RequestMapping( value="/items/whishlist",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<WhishlistListItem> findAllAsListItems() {
		List<Whishlist> list = whishlistService.findAll();
		List<WhishlistListItem> items = new LinkedList<WhishlistListItem>();
		for ( Whishlist whishlist : list ) {
			items.add(new WhishlistListItem( whishlist ) );
		}
		return items;
	}
	
	@RequestMapping( value="/whishlist",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<Whishlist> findAll() {
		return whishlistService.findAll();
	}

	@RequestMapping( value="/whishlist/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Whishlist findOne(@PathVariable("id") Integer id) {
		return whishlistService.findById(id);
	}
	
	@RequestMapping( value="/whishlist",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Whishlist create(@RequestBody Whishlist whishlist) {
		return whishlistService.create(whishlist);
	}

	@RequestMapping( value="/whishlist/{id}",
			method = RequestMethod.PUT,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Whishlist update(@PathVariable("id") Integer id, @RequestBody Whishlist whishlist) {
		return whishlistService.update(whishlist);
	}

	@RequestMapping( value="/whishlist/{id}",
			method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void delete(@PathVariable("id") Integer id) {
		whishlistService.delete(id);
	}
	
}
