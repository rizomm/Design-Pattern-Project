package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.capsoagritrancommerce.entity.jpa.CommandEntity;

/**
 * Repository : Command.
 */
public interface CommandJpaRepository extends PagingAndSortingRepository<CommandEntity, Integer> {

}
