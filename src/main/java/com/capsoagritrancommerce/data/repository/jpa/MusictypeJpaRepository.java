package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.capsoagritrancommerce.entity.jpa.MusictypeEntity;

/**
 * Repository : Musictype.
 */
public interface MusictypeJpaRepository extends PagingAndSortingRepository<MusictypeEntity, Integer> {

}
