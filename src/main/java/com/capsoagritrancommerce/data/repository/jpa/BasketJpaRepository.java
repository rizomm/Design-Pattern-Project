package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.capsoagritrancommerce.entity.jpa.BasketEntity;

/**
 * Repository : Basket.
 */
public interface BasketJpaRepository extends PagingAndSortingRepository<BasketEntity, Integer> {

}
