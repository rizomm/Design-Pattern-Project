package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.capsoagritrancommerce.entity.jpa.RatingEntity;

/**
 * Repository : Rating.
 */
public interface RatingJpaRepository extends PagingAndSortingRepository<RatingEntity, Integer> {

}
