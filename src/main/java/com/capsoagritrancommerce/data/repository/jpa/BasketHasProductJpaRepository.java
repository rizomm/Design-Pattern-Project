package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.capsoagritrancommerce.entity.jpa.BasketHasProductEntity;
import com.capsoagritrancommerce.entity.jpa.BasketHasProductEntityKey;

/**
 * Repository : BasketHasProduct.
 */
public interface BasketHasProductJpaRepository extends PagingAndSortingRepository<BasketHasProductEntity, BasketHasProductEntityKey> {

}
