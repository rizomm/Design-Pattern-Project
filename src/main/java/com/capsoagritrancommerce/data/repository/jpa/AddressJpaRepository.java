package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.capsoagritrancommerce.entity.jpa.AddressEntity;

/**
 * Repository : Address.
 */
public interface AddressJpaRepository extends PagingAndSortingRepository<AddressEntity, Integer> {

}
