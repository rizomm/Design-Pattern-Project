package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.capsoagritrancommerce.entity.jpa.ProductcategoryEntity;

/**
 * Repository : Productcategory.
 */
public interface ProductcategoryJpaRepository extends PagingAndSortingRepository<ProductcategoryEntity, Integer> {

}
