package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.capsoagritrancommerce.entity.jpa.DiscountEntity;

/**
 * Repository : Discount.
 */
public interface DiscountJpaRepository extends PagingAndSortingRepository<DiscountEntity, Integer> {

}
