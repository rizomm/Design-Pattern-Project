package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.capsoagritrancommerce.entity.jpa.ProductEntity;

/**
 * Repository : Product.
 */
public interface ProductJpaRepository extends PagingAndSortingRepository<ProductEntity, Integer> {

	Iterable<ProductEntity> findByNameIgnoreCaseContaining(String title);

	Iterable<ProductEntity> findByDescriptionIgnoreCaseContaining(String description);

	Iterable<ProductEntity> findByRefIgnoreCaseContaining(String ref);

	Iterable<ProductEntity> findByNameIgnoreCaseContainingOrDescriptionIgnoreCaseContainingOrRefIgnoreCaseContaining(String search_word, String search_word2, String search_word3);

	
}
