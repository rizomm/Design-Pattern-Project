package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.capsoagritrancommerce.entity.jpa.StatusEntity;

/**
 * Repository : Status.
 */
public interface StatusJpaRepository extends PagingAndSortingRepository<StatusEntity, Integer> {

}
