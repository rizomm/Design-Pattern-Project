package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.capsoagritrancommerce.entity.jpa.CommandHasProductEntity;
import com.capsoagritrancommerce.entity.jpa.CommandHasProductEntityKey;

/**
 * Repository : CommandHasProduct.
 */
public interface CommandHasProductJpaRepository extends PagingAndSortingRepository<CommandHasProductEntity, CommandHasProductEntityKey> {

}
