package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.capsoagritrancommerce.entity.jpa.WhishlistEntity;

/**
 * Repository : Whishlist.
 */
public interface WhishlistJpaRepository extends PagingAndSortingRepository<WhishlistEntity, Integer> {

}
