package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.capsoagritrancommerce.entity.jpa.UserEntity;
import com.capsoagritrancommerce.entity.jpa.UserRolesEntity;

/**
 * Repository : UserRoles.
 */
public interface UserRolesJpaRepository extends PagingAndSortingRepository<UserRolesEntity, Integer> {

	Iterable<UserRolesEntity> findByUsername(String email);

	UserRolesEntity findByRole(String role);

}
