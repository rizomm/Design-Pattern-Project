package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.capsoagritrancommerce.entity.jpa.ArtistEntity;

/**
 * Repository : Artist.
 */
public interface ArtistJpaRepository extends PagingAndSortingRepository<ArtistEntity, Integer> {

}
