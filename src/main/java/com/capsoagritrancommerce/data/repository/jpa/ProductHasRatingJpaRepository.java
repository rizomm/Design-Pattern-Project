package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.capsoagritrancommerce.entity.jpa.ProductHasRatingEntity;
import com.capsoagritrancommerce.entity.jpa.ProductHasRatingEntityKey;

/**
 * Repository : ProductHasRating.
 */
public interface ProductHasRatingJpaRepository extends PagingAndSortingRepository<ProductHasRatingEntity, ProductHasRatingEntityKey> {

}
