package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.capsoagritrancommerce.entity.jpa.UserHasAddressEntity;
import com.capsoagritrancommerce.entity.jpa.UserHasAddressEntityKey;

/**
 * Repository : UserHasAddress.
 */
public interface UserHasAddressJpaRepository extends PagingAndSortingRepository<UserHasAddressEntity, UserHasAddressEntityKey> {

}
