package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.capsoagritrancommerce.entity.jpa.TypePayEntity;

/**
 * Repository : TypePay.
 */
public interface TypePayJpaRepository extends PagingAndSortingRepository<TypePayEntity, Integer> {

}
