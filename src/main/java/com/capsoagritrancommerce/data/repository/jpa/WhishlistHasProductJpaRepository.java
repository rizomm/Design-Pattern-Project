package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.capsoagritrancommerce.entity.jpa.WhishlistHasProductEntity;
import com.capsoagritrancommerce.entity.jpa.WhishlistHasProductEntityKey;

/**
 * Repository : WhishlistHasProduct.
 */
public interface WhishlistHasProductJpaRepository extends PagingAndSortingRepository<WhishlistHasProductEntity, WhishlistHasProductEntityKey> {


}
