package com.capsoagritrancommerce.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.capsoagritrancommerce.entity.jpa.CountryEntity;

/**
 * Repository : Country.
 */
public interface CountryJpaRepository extends PagingAndSortingRepository<CountryEntity, Integer> {

}
