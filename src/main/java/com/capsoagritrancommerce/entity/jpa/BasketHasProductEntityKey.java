/*
 * Created on 8 mars 2015 ( Time 12:20:15 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.capsoagritrancommerce.entity.jpa;
import java.io.Serializable;

import javax.persistence.*;

/**
 * Composite primary key for entity "BasketHasProductEntity" ( stored in table "basket_has_product" )
 *
 * @author Telosys Tools Generator
 *
 */
 @Embeddable
public class BasketHasProductEntityKey implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY KEY ATTRIBUTES 
    //----------------------------------------------------------------------
    @Column(name="basket_id", nullable=false)
    private Integer    basketId     ;
    
    @Column(name="product_id", nullable=false)
    private Integer    productId    ;
    

    //----------------------------------------------------------------------
    // CONSTRUCTORS
    //----------------------------------------------------------------------
    public BasketHasProductEntityKey() {
        super();
    }

    public BasketHasProductEntityKey( Integer basketId, Integer productId ) {
        super();
        this.basketId = basketId ;
        this.productId = productId ;
    }
    
    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR KEY FIELDS
    //----------------------------------------------------------------------
    public void setBasketId( Integer value ) {
        this.basketId = value;
    }
    public Integer getBasketId() {
        return this.basketId;
    }

    public void setProductId( Integer value ) {
        this.productId = value;
    }
    public Integer getProductId() {
        return this.productId;
    }


    //----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		BasketHasProductEntityKey other = (BasketHasProductEntityKey) obj; 
		//--- Attribute basketId
		if ( basketId == null ) { 
			if ( other.basketId != null ) 
				return false ; 
		} else if ( ! basketId.equals(other.basketId) ) 
			return false ; 
		//--- Attribute productId
		if ( productId == null ) { 
			if ( other.productId != null ) 
				return false ; 
		} else if ( ! productId.equals(other.productId) ) 
			return false ; 
		return true; 
	} 


    //----------------------------------------------------------------------
    // hashCode METHOD
    //----------------------------------------------------------------------
	public int hashCode() { 
		final int prime = 31; 
		int result = 1; 
		
		//--- Attribute basketId
		result = prime * result + ((basketId == null) ? 0 : basketId.hashCode() ) ; 
		//--- Attribute productId
		result = prime * result + ((productId == null) ? 0 : productId.hashCode() ) ; 
		
		return result; 
	} 


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append(basketId); 
		sb.append("|"); 
		sb.append(productId); 
        return sb.toString();
    }
}
