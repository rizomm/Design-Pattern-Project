/*
 * Created on 8 mars 2015 ( Time 12:20:16 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.capsoagritrancommerce.entity.jpa;
import java.io.Serializable;

import javax.persistence.*;

/**
 * Composite primary key for entity "CommandHasProductEntity" ( stored in table "command_has_product" )
 *
 * @author Telosys Tools Generator
 *
 */
 @Embeddable
public class CommandHasProductEntityKey implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY KEY ATTRIBUTES 
    //----------------------------------------------------------------------
    @Column(name="command_id", nullable=false)
    private Integer    commandId    ;
    
    @Column(name="product_id", nullable=false)
    private Integer    productId    ;
    

    //----------------------------------------------------------------------
    // CONSTRUCTORS
    //----------------------------------------------------------------------
    public CommandHasProductEntityKey() {
        super();
    }

    public CommandHasProductEntityKey( Integer commandId, Integer productId ) {
        super();
        this.commandId = commandId ;
        this.productId = productId ;
    }
    
    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR KEY FIELDS
    //----------------------------------------------------------------------
    public void setCommandId( Integer value ) {
        this.commandId = value;
    }
    public Integer getCommandId() {
        return this.commandId;
    }

    public void setProductId( Integer value ) {
        this.productId = value;
    }
    public Integer getProductId() {
        return this.productId;
    }


    //----------------------------------------------------------------------
    // equals METHOD
    //----------------------------------------------------------------------
	public boolean equals(Object obj) { 
		if ( this == obj ) return true ; 
		if ( obj == null ) return false ;
		if ( this.getClass() != obj.getClass() ) return false ; 
		CommandHasProductEntityKey other = (CommandHasProductEntityKey) obj; 
		//--- Attribute commandId
		if ( commandId == null ) { 
			if ( other.commandId != null ) 
				return false ; 
		} else if ( ! commandId.equals(other.commandId) ) 
			return false ; 
		//--- Attribute productId
		if ( productId == null ) { 
			if ( other.productId != null ) 
				return false ; 
		} else if ( ! productId.equals(other.productId) ) 
			return false ; 
		return true; 
	} 


    //----------------------------------------------------------------------
    // hashCode METHOD
    //----------------------------------------------------------------------
	public int hashCode() { 
		final int prime = 31; 
		int result = 1; 
		
		//--- Attribute commandId
		result = prime * result + ((commandId == null) ? 0 : commandId.hashCode() ) ; 
		//--- Attribute productId
		result = prime * result + ((productId == null) ? 0 : productId.hashCode() ) ; 
		
		return result; 
	} 


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() {
		StringBuffer sb = new StringBuffer(); 
		sb.append(commandId); 
		sb.append("|"); 
		sb.append(productId); 
        return sb.toString();
    }
}
