/*
 * Created on 8 mars 2015 ( Time 12:20:07 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.capsoagritrancommerce.web.listitem;

import com.capsoagritrancommerce.entity.CommandHasProduct;
import com.capsoagritrancommerce.web.common.ListItem;

public class CommandHasProductListItem implements ListItem {

	private final String value ;
	private final String label ;
	
	public CommandHasProductListItem(CommandHasProduct commandHasProduct) {
		super();

		this.value = ""
			 + commandHasProduct.getCommandId()
			 + "|"  + commandHasProduct.getProductId()
		;

		//TODO : Define here the attributes to be displayed as the label
		this.label = commandHasProduct.toString();
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public String getLabel() {
		return label;
	}

}
