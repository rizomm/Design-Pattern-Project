/*
 * Created on 8 mars 2015 ( Time 12:20:07 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.capsoagritrancommerce.web.listitem;

import com.capsoagritrancommerce.entity.Country;
import com.capsoagritrancommerce.web.common.ListItem;

public class CountryListItem implements ListItem {

	private final String value ;
	private final String label ;
	
	public CountryListItem(Country country) {
		super();

		this.value = ""
			 + country.getId()
		;

		//TODO : Define here the attributes to be displayed as the label
		this.label = country.toString();
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public String getLabel() {
		return label;
	}

}
