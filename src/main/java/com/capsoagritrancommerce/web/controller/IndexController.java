package com.capsoagritrancommerce.web.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.capsoagritrancommerce.business.service.UserRolesService;
import com.capsoagritrancommerce.business.service.UserService;
import com.capsoagritrancommerce.entity.User;
import com.capsoagritrancommerce.entity.UserRoles;
 
@Controller
public class IndexController {
	
	@Resource
    private UserRolesService userRolesService;
 
	@RequestMapping(value = "")
	public ModelAndView index() {
 
		ModelAndView model = new ModelAndView("index");
		
		Boolean isLogged 	= false ;
		Boolean isUser 		= false ;
		Boolean isAdmin		= false ;
		
	
		// On test si il y a un user connecté
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication(); 
	    
	    if(auth.getName() != "anonymousUser"){
	    	
	    	isLogged 				= true;
	    	 
	    	//On recupere la liste des roles du ser
	    	 List<UserRoles> userRoles 	= userRolesService.findByUsername(auth.getName());
	    	 	    	 
	    	 //On boucle sur la liste des roles du user et si on trouve le ROLE_USER ou le ROLE_ADMIN, on met les flags a true
	    	 for( UserRoles role : userRoles ){
	    		 if(role.getRole().contentEquals("ROLE_USER")){
	    			 isUser		= true;  
	    		 }
	    		 if(role.getRole().contentEquals("ROLE_ADMIN")){
	    			 isAdmin	= true;  
	    		 }
	    		 
	    	 }
	    	
	    }
	    
		model.addObject("isLogged", isLogged);
		model.addObject("isUser", isUser);
		model.addObject("isAdmin", isAdmin);

 
		return model;
 
	}
 
}