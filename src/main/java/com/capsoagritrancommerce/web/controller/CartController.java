package com.capsoagritrancommerce.web.controller;

import com.capsoagritrancommerce.business.service.ProductService;
import com.capsoagritrancommerce.entity.Cart;
import com.capsoagritrancommerce.entity.Musictype;
import com.capsoagritrancommerce.entity.Product;
import com.capsoagritrancommerce.web.common.AbstractController;
import com.capsoagritrancommerce.web.listitem.MusictypeListItem;

import org.hsqldb.SessionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ecockenp on 20/03/2015.
 */
@Controller
@Scope("session")
@RequestMapping( "/cart" )
public class CartController extends AbstractController {

    //--- Variables names ( to be used in JSP with Expression Language )
    private static final String MAIN_ENTITY_NAME = "cart";
    private static final String MAIN_LIST_NAME   = "list";

    //--- JSP pages names ( View name in the MVC model )
    private static final String JSP_FORM   = "basket/form";
    private static final String JSP_LIST   = "basket/list";
    private static final String JSP_BASKET = "basket/page";

    //--- SAVE ACTION ( in the HTML form )
    private static final String SAVE_ACTION_CREATE = "/basket/create";
    private static final String SAVE_ACTION_UPDATE = "/basket/update";

    @Resource
    private ProductService productService; // Injected by Spring

    /**
     * Default constructor
     */
    public CartController() {
        super( CartController.class, MAIN_ENTITY_NAME );
        log( "BasketController created." );
    }

    Cart          cart        = new Cart();
    List<Product> productList = new ArrayList<Product>();

    /**
     * VIEW THE CART
     */
    @RequestMapping( value = "/basketpage/{productId}" )
    public ModelAndView basketPage( @PathVariable( "productId" ) int productId,Model model ) {

    	ModelAndView basketPage = new ModelAndView(JSP_BASKET) ;
    	
    	
        Product product = productService.findById( productId );
        
        Boolean inList = false ;
		for ( Product productInList : productList ) {
			if(productInList.getId() == product.getId()){
				inList = true ;
			}
		}
		
		if(!inList){
			productList.add( product );
		}
        
		
        cart.setProductList( productList );
     
        basketPage.addObject("productList" , productList );

        return basketPage;
    }
    
    @RequestMapping( value = "/basketpage" )
    public ModelAndView basketPage() {

    	ModelAndView basketPage = new ModelAndView(JSP_BASKET) ;
    	
    	List<Product> productList = cart.getProductList() ;
     
        basketPage.addObject("productList" , productList );

        return basketPage;
    }
}
