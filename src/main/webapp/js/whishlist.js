function addWhishlist() { 

		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
			
			pathArray = location.href.split( '/' );
			protocol = pathArray[0];
			host = pathArray[2];
			appname = pathArray[3];
			baseurl = protocol + '//' + host + '/' + appname ;
			
			var productid 	= $("#productid").val();
			var whishlistid = $("#whishlistSelect").val();			

			$.ajax({
				type: "GET",
				url: baseurl + "/whishlist/add",
		        beforeSend: function(xhr) {
		            xhr.setRequestHeader(header, token);
		        },
				data: 
					{ 
						productid : productid,
						whishlistid : whishlistid,
					},
			    success: function(){
			    		alert("Produit ajouté à la whishlist");
			    		$('#whishListModal').modal('hide') ;
			    	},
                error: function() { 
                    alert("Le produit existe déjà dans la liste"); 
                    $('#whishListModal').modal('hide') ;
                }  
				});
} 