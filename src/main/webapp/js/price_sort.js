function priceSorter(a, b) {
    a = +a.slice(0,-2); // remove " €"
    b = +b.slice(0,-2);
    if (a > b) return 1;
    if (a < b) return -1;
    return 0;
}