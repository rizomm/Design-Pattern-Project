<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />

    <c:set var="baseurl" value="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}" />

<form action="${baseurl}/product/" methode="post">
	<div class="form-group">
		<input id="search_word" name="search_word" class="form-control" placeholder="<s:message code="search"/>"/>
	</div>
</form>
