<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />

<c:set var="baseurl" value="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}" />

		<div class="container">
			<div class="row">				
				<div class="col-sm-12 padding-right">
					<form action="${baseurl}/product/" methode="post">
						<div class="form-group col-sm-3">
							<label class="col-sm-4 control-label">Title: </label>
							<div class="col-sm-8">
		  						<input id="product_title" name="product_title" class="form-control"  />
							</div>
						</div>
						<div class="form-group col-sm-4">
							<label class="col-sm-4 control-label">Description: </label>
							<div class="col-sm-8">
		  						<input id="product_description" name="product_description" class="form-control"  />
							</div>
						</div>
						<div class="form-group col-sm-3">
							<label class="col-sm-4 control-label">Ref: </label>
							<div class="col-sm-8">
		  						<input id="product_ref" name="product_ref" class="form-control"  />
							</div>
						</div>												
						
						<button type="submit" class="btn btn-info" />
							<i class="fa fa-search">
								<s:message code="search"/>
							</i>
						</button>
						
					</form>
				</div>
			</div>
		</div>