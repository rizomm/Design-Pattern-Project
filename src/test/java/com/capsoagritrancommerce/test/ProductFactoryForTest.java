package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.Product;

public class ProductFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Product newProduct() {

		Integer id = mockValues.nextInteger();

		Product product = new Product();
		product.setId(id);
		return product;
	}
	
}
