package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.ArtistEntity;

public class ArtistEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public ArtistEntity newArtistEntity() {

		Integer id = mockValues.nextInteger();

		ArtistEntity artistEntity = new ArtistEntity();
		artistEntity.setId(id);
		return artistEntity;
	}
	
}
