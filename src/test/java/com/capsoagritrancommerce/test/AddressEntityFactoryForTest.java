package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.AddressEntity;

public class AddressEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public AddressEntity newAddressEntity() {

		Integer id = mockValues.nextInteger();

		AddressEntity addressEntity = new AddressEntity();
		addressEntity.setId(id);
		return addressEntity;
	}
	
}
