package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.RatingEntity;

public class RatingEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public RatingEntity newRatingEntity() {

		Integer id = mockValues.nextInteger();

		RatingEntity ratingEntity = new RatingEntity();
		ratingEntity.setId(id);
		return ratingEntity;
	}
	
}
