package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.Musictype;

public class MusictypeFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Musictype newMusictype() {

		Integer id = mockValues.nextInteger();

		Musictype musictype = new Musictype();
		musictype.setId(id);
		return musictype;
	}
	
}
