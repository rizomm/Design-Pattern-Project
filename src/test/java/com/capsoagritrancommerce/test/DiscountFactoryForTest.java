package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.Discount;

public class DiscountFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Discount newDiscount() {

		Integer id = mockValues.nextInteger();

		Discount discount = new Discount();
		discount.setId(id);
		return discount;
	}
	
}
