package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.ProductEntity;

public class ProductEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public ProductEntity newProductEntity() {

		Integer id = mockValues.nextInteger();

		ProductEntity productEntity = new ProductEntity();
		productEntity.setId(id);
		return productEntity;
	}
	
}
