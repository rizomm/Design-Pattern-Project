package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.Whishlist;

public class WhishlistFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Whishlist newWhishlist() {

		Integer id = mockValues.nextInteger();

		Whishlist whishlist = new Whishlist();
		whishlist.setId(id);
		return whishlist;
	}
	
}
