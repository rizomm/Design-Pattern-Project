package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.CommandHasProductEntity;

public class CommandHasProductEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public CommandHasProductEntity newCommandHasProductEntity() {

		Integer commandId = mockValues.nextInteger();
		Integer productId = mockValues.nextInteger();

		CommandHasProductEntity commandHasProductEntity = new CommandHasProductEntity();
		commandHasProductEntity.setCommandId(commandId);
		commandHasProductEntity.setProductId(productId);
		return commandHasProductEntity;
	}
	
}
