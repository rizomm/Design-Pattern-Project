package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.WhishlistHasProduct;

public class WhishlistHasProductFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public WhishlistHasProduct newWhishlistHasProduct() {

		Integer whishlistId = mockValues.nextInteger();
		Integer productId = mockValues.nextInteger();

		WhishlistHasProduct whishlistHasProduct = new WhishlistHasProduct();
		whishlistHasProduct.setWhishlistId(whishlistId);
		whishlistHasProduct.setProductId(productId);
		return whishlistHasProduct;
	}
	
}
