package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.ProductHasRatingEntity;

public class ProductHasRatingEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public ProductHasRatingEntity newProductHasRatingEntity() {

		Integer productId = mockValues.nextInteger();
		Integer ratingId = mockValues.nextInteger();

		ProductHasRatingEntity productHasRatingEntity = new ProductHasRatingEntity();
		productHasRatingEntity.setProductId(productId);
		productHasRatingEntity.setRatingId(ratingId);
		return productHasRatingEntity;
	}
	
}
