package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.WhishlistEntity;

public class WhishlistEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public WhishlistEntity newWhishlistEntity() {

		Integer id = mockValues.nextInteger();

		WhishlistEntity whishlistEntity = new WhishlistEntity();
		whishlistEntity.setId(id);
		return whishlistEntity;
	}
	
}
