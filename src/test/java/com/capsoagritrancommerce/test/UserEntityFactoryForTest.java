package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.UserEntity;

public class UserEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public UserEntity newUserEntity() {

		Integer id = mockValues.nextInteger();

		UserEntity userEntity = new UserEntity();
		userEntity.setId(id);
		return userEntity;
	}
	
}
