package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.CommandEntity;

public class CommandEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public CommandEntity newCommandEntity() {

		Integer id = mockValues.nextInteger();

		CommandEntity commandEntity = new CommandEntity();
		commandEntity.setId(id);
		return commandEntity;
	}
	
}
