package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.BasketHasProductEntity;

public class BasketHasProductEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public BasketHasProductEntity newBasketHasProductEntity() {

		Integer basketId = mockValues.nextInteger();
		Integer productId = mockValues.nextInteger();

		BasketHasProductEntity basketHasProductEntity = new BasketHasProductEntity();
		basketHasProductEntity.setBasketId(basketId);
		basketHasProductEntity.setProductId(productId);
		return basketHasProductEntity;
	}
	
}
