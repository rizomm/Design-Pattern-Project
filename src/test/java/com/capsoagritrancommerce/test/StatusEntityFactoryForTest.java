package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.StatusEntity;

public class StatusEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public StatusEntity newStatusEntity() {

		Integer id = mockValues.nextInteger();

		StatusEntity statusEntity = new StatusEntity();
		statusEntity.setId(id);
		return statusEntity;
	}
	
}
