package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.Country;

public class CountryFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Country newCountry() {

		Integer id = mockValues.nextInteger();

		Country country = new Country();
		country.setId(id);
		return country;
	}
	
}
