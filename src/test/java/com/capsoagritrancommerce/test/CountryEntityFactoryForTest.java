package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.CountryEntity;

public class CountryEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public CountryEntity newCountryEntity() {

		Integer id = mockValues.nextInteger();

		CountryEntity countryEntity = new CountryEntity();
		countryEntity.setId(id);
		return countryEntity;
	}
	
}
