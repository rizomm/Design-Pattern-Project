package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.BasketEntity;

public class BasketEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public BasketEntity newBasketEntity() {

		Integer id = mockValues.nextInteger();

		BasketEntity basketEntity = new BasketEntity();
		basketEntity.setId(id);
		return basketEntity;
	}
	
}
