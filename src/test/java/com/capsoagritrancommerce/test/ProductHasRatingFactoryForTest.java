package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.ProductHasRating;

public class ProductHasRatingFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public ProductHasRating newProductHasRating() {

		Integer productId = mockValues.nextInteger();
		Integer ratingId = mockValues.nextInteger();

		ProductHasRating productHasRating = new ProductHasRating();
		productHasRating.setProductId(productId);
		productHasRating.setRatingId(ratingId);
		return productHasRating;
	}
	
}
