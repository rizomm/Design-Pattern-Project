package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.DiscountEntity;

public class DiscountEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public DiscountEntity newDiscountEntity() {

		Integer id = mockValues.nextInteger();

		DiscountEntity discountEntity = new DiscountEntity();
		discountEntity.setId(id);
		return discountEntity;
	}
	
}
