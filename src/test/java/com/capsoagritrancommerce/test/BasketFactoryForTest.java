package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.Basket;

public class BasketFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Basket newBasket() {

		Integer id = mockValues.nextInteger();

		Basket basket = new Basket();
		basket.setId(id);
		return basket;
	}
	
}
