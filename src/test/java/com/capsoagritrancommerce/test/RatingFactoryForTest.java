package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.Rating;

public class RatingFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Rating newRating() {

		Integer id = mockValues.nextInteger();

		Rating rating = new Rating();
		rating.setId(id);
		return rating;
	}
	
}
