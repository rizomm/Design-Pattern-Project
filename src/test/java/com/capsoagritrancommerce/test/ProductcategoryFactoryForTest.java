package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.Productcategory;

public class ProductcategoryFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Productcategory newProductcategory() {

		Integer id = mockValues.nextInteger();

		Productcategory productcategory = new Productcategory();
		productcategory.setId(id);
		return productcategory;
	}
	
}
