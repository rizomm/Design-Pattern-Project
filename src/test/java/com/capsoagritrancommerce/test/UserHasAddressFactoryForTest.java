package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.UserHasAddress;

public class UserHasAddressFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public UserHasAddress newUserHasAddress() {

		Integer userId = mockValues.nextInteger();
		Integer addressId = mockValues.nextInteger();

		UserHasAddress userHasAddress = new UserHasAddress();
		userHasAddress.setUserId(userId);
		userHasAddress.setAddressId(addressId);
		return userHasAddress;
	}
	
}
