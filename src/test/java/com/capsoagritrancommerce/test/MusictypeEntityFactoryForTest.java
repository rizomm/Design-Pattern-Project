package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.MusictypeEntity;

public class MusictypeEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public MusictypeEntity newMusictypeEntity() {

		Integer id = mockValues.nextInteger();

		MusictypeEntity musictypeEntity = new MusictypeEntity();
		musictypeEntity.setId(id);
		return musictypeEntity;
	}
	
}
