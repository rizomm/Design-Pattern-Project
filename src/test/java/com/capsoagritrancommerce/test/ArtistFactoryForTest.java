package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.Artist;

public class ArtistFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Artist newArtist() {

		Integer id = mockValues.nextInteger();

		Artist artist = new Artist();
		artist.setId(id);
		return artist;
	}
	
}
