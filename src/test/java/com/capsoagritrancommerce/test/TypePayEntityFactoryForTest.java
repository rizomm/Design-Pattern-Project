package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.TypePayEntity;

public class TypePayEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public TypePayEntity newTypePayEntity() {

		Integer id = mockValues.nextInteger();

		TypePayEntity typePayEntity = new TypePayEntity();
		typePayEntity.setId(id);
		return typePayEntity;
	}
	
}
