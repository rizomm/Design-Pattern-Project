package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.WhishlistHasProductEntity;

public class WhishlistHasProductEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public WhishlistHasProductEntity newWhishlistHasProductEntity() {

		Integer whishlistId = mockValues.nextInteger();
		Integer productId = mockValues.nextInteger();

		WhishlistHasProductEntity whishlistHasProductEntity = new WhishlistHasProductEntity();
		whishlistHasProductEntity.setWhishlistId(whishlistId);
		whishlistHasProductEntity.setProductId(productId);
		return whishlistHasProductEntity;
	}
	
}
