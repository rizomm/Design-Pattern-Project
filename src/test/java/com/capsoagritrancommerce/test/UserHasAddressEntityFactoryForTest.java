package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.UserHasAddressEntity;

public class UserHasAddressEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public UserHasAddressEntity newUserHasAddressEntity() {

		Integer userId = mockValues.nextInteger();
		Integer addressId = mockValues.nextInteger();

		UserHasAddressEntity userHasAddressEntity = new UserHasAddressEntity();
		userHasAddressEntity.setUserId(userId);
		userHasAddressEntity.setAddressId(addressId);
		return userHasAddressEntity;
	}
	
}
