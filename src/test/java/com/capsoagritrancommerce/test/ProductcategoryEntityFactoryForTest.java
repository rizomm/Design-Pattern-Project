package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.jpa.ProductcategoryEntity;

public class ProductcategoryEntityFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public ProductcategoryEntity newProductcategoryEntity() {

		Integer id = mockValues.nextInteger();

		ProductcategoryEntity productcategoryEntity = new ProductcategoryEntity();
		productcategoryEntity.setId(id);
		return productcategoryEntity;
	}
	
}
