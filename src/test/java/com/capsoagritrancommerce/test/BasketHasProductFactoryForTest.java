package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.BasketHasProduct;

public class BasketHasProductFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public BasketHasProduct newBasketHasProduct() {

		Integer basketId = mockValues.nextInteger();
		Integer productId = mockValues.nextInteger();

		BasketHasProduct basketHasProduct = new BasketHasProduct();
		basketHasProduct.setBasketId(basketId);
		basketHasProduct.setProductId(productId);
		return basketHasProduct;
	}
	
}
