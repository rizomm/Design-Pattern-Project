package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.Address;

public class AddressFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Address newAddress() {

		Integer id = mockValues.nextInteger();

		Address address = new Address();
		address.setId(id);
		return address;
	}
	
}
