package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.TypePay;

public class TypePayFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public TypePay newTypePay() {

		Integer id = mockValues.nextInteger();

		TypePay typePay = new TypePay();
		typePay.setId(id);
		return typePay;
	}
	
}
