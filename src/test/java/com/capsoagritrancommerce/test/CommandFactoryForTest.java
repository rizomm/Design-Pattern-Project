package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.Command;

public class CommandFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public Command newCommand() {

		Integer id = mockValues.nextInteger();

		Command command = new Command();
		command.setId(id);
		return command;
	}
	
}
