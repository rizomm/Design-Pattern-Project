package com.capsoagritrancommerce.test;

import com.capsoagritrancommerce.entity.CommandHasProduct;

public class CommandHasProductFactoryForTest {

	private MockValues mockValues = new MockValues();
	
	public CommandHasProduct newCommandHasProduct() {

		Integer commandId = mockValues.nextInteger();
		Integer productId = mockValues.nextInteger();

		CommandHasProduct commandHasProduct = new CommandHasProduct();
		commandHasProduct.setCommandId(commandId);
		commandHasProduct.setProductId(productId);
		return commandHasProduct;
	}
	
}
