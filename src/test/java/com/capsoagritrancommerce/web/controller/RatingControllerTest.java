package com.capsoagritrancommerce.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.capsoagritrancommerce.entity.Rating;
import com.capsoagritrancommerce.test.RatingFactoryForTest;

//--- Services 
import com.capsoagritrancommerce.business.service.RatingService;


import com.capsoagritrancommerce.web.common.Message;
import com.capsoagritrancommerce.web.common.MessageHelper;
import com.capsoagritrancommerce.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class RatingControllerTest {
	
	@InjectMocks
	private RatingController ratingController;
	@Mock
	private RatingService ratingService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;

	private RatingFactoryForTest ratingFactoryForTest = new RatingFactoryForTest();


	private void givenPopulateModel() {
	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<Rating> list = new ArrayList<Rating>();
		when(ratingService.findAll()).thenReturn(list);
		
		// When
		String viewName = ratingController.list(model);
		
		// Then
		assertEquals("rating/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = ratingController.formForCreate(model);
		
		// Then
		assertEquals("rating/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((Rating)modelMap.get("rating")).getId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/rating/create", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Rating rating = ratingFactoryForTest.newRating();
		Integer id = rating.getId();
		when(ratingService.findById(id)).thenReturn(rating);
		
		// When
		String viewName = ratingController.formForUpdate(model, id);
		
		// Then
		assertEquals("rating/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(rating, (Rating) modelMap.get("rating"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/rating/update", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Rating rating = ratingFactoryForTest.newRating();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Rating ratingCreated = new Rating();
		when(ratingService.create(rating)).thenReturn(ratingCreated); 
		
		// When
		String viewName = ratingController.create(rating, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/rating/form/"+rating.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(ratingCreated, (Rating) modelMap.get("rating"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Rating rating = ratingFactoryForTest.newRating();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = ratingController.create(rating, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("rating/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(rating, (Rating) modelMap.get("rating"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/rating/create", modelMap.get("saveAction"));
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Rating rating = ratingFactoryForTest.newRating();
		
		Exception exception = new RuntimeException("test exception");
		when(ratingService.create(rating)).thenThrow(exception);
		
		// When
		String viewName = ratingController.create(rating, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("rating/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(rating, (Rating) modelMap.get("rating"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/rating/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "rating.error.create", exception);
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Rating rating = ratingFactoryForTest.newRating();
		Integer id = rating.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Rating ratingSaved = new Rating();
		ratingSaved.setId(id);
		when(ratingService.update(rating)).thenReturn(ratingSaved); 
		
		// When
		String viewName = ratingController.update(rating, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/rating/form/"+rating.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(ratingSaved, (Rating) modelMap.get("rating"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Rating rating = ratingFactoryForTest.newRating();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = ratingController.update(rating, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("rating/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(rating, (Rating) modelMap.get("rating"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/rating/update", modelMap.get("saveAction"));
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Rating rating = ratingFactoryForTest.newRating();
		
		Exception exception = new RuntimeException("test exception");
		when(ratingService.update(rating)).thenThrow(exception);
		
		// When
		String viewName = ratingController.update(rating, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("rating/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(rating, (Rating) modelMap.get("rating"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/rating/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "rating.error.update", exception);
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Rating rating = ratingFactoryForTest.newRating();
		Integer id = rating.getId();
		
		// When
		String viewName = ratingController.delete(redirectAttributes, id);
		
		// Then
		verify(ratingService).delete(id);
		assertEquals("redirect:/rating", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Rating rating = ratingFactoryForTest.newRating();
		Integer id = rating.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(ratingService).delete(id);
		
		// When
		String viewName = ratingController.delete(redirectAttributes, id);
		
		// Then
		verify(ratingService).delete(id);
		assertEquals("redirect:/rating", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "rating.error.delete", exception);
	}
	
	
}
