package com.capsoagritrancommerce.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.capsoagritrancommerce.entity.Discount;
import com.capsoagritrancommerce.test.DiscountFactoryForTest;

//--- Services 
import com.capsoagritrancommerce.business.service.DiscountService;


import com.capsoagritrancommerce.web.common.Message;
import com.capsoagritrancommerce.web.common.MessageHelper;
import com.capsoagritrancommerce.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class DiscountControllerTest {
	
	@InjectMocks
	private DiscountController discountController;
	@Mock
	private DiscountService discountService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;

	private DiscountFactoryForTest discountFactoryForTest = new DiscountFactoryForTest();


	private void givenPopulateModel() {
	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<Discount> list = new ArrayList<Discount>();
		when(discountService.findAll()).thenReturn(list);
		
		// When
		String viewName = discountController.list(model);
		
		// Then
		assertEquals("discount/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = discountController.formForCreate(model);
		
		// Then
		assertEquals("discount/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((Discount)modelMap.get("discount")).getId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/discount/create", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Discount discount = discountFactoryForTest.newDiscount();
		Integer id = discount.getId();
		when(discountService.findById(id)).thenReturn(discount);
		
		// When
		String viewName = discountController.formForUpdate(model, id);
		
		// Then
		assertEquals("discount/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(discount, (Discount) modelMap.get("discount"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/discount/update", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Discount discount = discountFactoryForTest.newDiscount();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Discount discountCreated = new Discount();
		when(discountService.create(discount)).thenReturn(discountCreated); 
		
		// When
		String viewName = discountController.create(discount, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/discount/form/"+discount.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(discountCreated, (Discount) modelMap.get("discount"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Discount discount = discountFactoryForTest.newDiscount();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = discountController.create(discount, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("discount/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(discount, (Discount) modelMap.get("discount"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/discount/create", modelMap.get("saveAction"));
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Discount discount = discountFactoryForTest.newDiscount();
		
		Exception exception = new RuntimeException("test exception");
		when(discountService.create(discount)).thenThrow(exception);
		
		// When
		String viewName = discountController.create(discount, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("discount/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(discount, (Discount) modelMap.get("discount"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/discount/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "discount.error.create", exception);
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Discount discount = discountFactoryForTest.newDiscount();
		Integer id = discount.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Discount discountSaved = new Discount();
		discountSaved.setId(id);
		when(discountService.update(discount)).thenReturn(discountSaved); 
		
		// When
		String viewName = discountController.update(discount, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/discount/form/"+discount.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(discountSaved, (Discount) modelMap.get("discount"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Discount discount = discountFactoryForTest.newDiscount();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = discountController.update(discount, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("discount/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(discount, (Discount) modelMap.get("discount"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/discount/update", modelMap.get("saveAction"));
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Discount discount = discountFactoryForTest.newDiscount();
		
		Exception exception = new RuntimeException("test exception");
		when(discountService.update(discount)).thenThrow(exception);
		
		// When
		String viewName = discountController.update(discount, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("discount/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(discount, (Discount) modelMap.get("discount"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/discount/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "discount.error.update", exception);
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Discount discount = discountFactoryForTest.newDiscount();
		Integer id = discount.getId();
		
		// When
		String viewName = discountController.delete(redirectAttributes, id);
		
		// Then
		verify(discountService).delete(id);
		assertEquals("redirect:/discount", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Discount discount = discountFactoryForTest.newDiscount();
		Integer id = discount.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(discountService).delete(id);
		
		// When
		String viewName = discountController.delete(redirectAttributes, id);
		
		// Then
		verify(discountService).delete(id);
		assertEquals("redirect:/discount", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "discount.error.delete", exception);
	}
	
	
}
