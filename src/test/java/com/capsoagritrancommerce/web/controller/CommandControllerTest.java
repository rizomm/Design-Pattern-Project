package com.capsoagritrancommerce.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.capsoagritrancommerce.entity.Command;
import com.capsoagritrancommerce.entity.Status;
import com.capsoagritrancommerce.entity.User;
import com.capsoagritrancommerce.entity.Discount;
import com.capsoagritrancommerce.entity.TypePay;
import com.capsoagritrancommerce.test.CommandFactoryForTest;
import com.capsoagritrancommerce.test.StatusFactoryForTest;
import com.capsoagritrancommerce.test.UserFactoryForTest;
import com.capsoagritrancommerce.test.DiscountFactoryForTest;
import com.capsoagritrancommerce.test.TypePayFactoryForTest;

//--- Services 
import com.capsoagritrancommerce.business.service.CommandService;
import com.capsoagritrancommerce.business.service.StatusService;
import com.capsoagritrancommerce.business.service.UserService;
import com.capsoagritrancommerce.business.service.DiscountService;
import com.capsoagritrancommerce.business.service.TypePayService;

//--- List Items 
import com.capsoagritrancommerce.web.listitem.StatusListItem;
import com.capsoagritrancommerce.web.listitem.UserListItem;
import com.capsoagritrancommerce.web.listitem.DiscountListItem;
import com.capsoagritrancommerce.web.listitem.TypePayListItem;

import com.capsoagritrancommerce.web.common.Message;
import com.capsoagritrancommerce.web.common.MessageHelper;
import com.capsoagritrancommerce.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class CommandControllerTest {
	
	@InjectMocks
	private CommandController commandController;
	@Mock
	private CommandService commandService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;
	@Mock
	private StatusService statusService; // Injected by Spring
	@Mock
	private UserService userService; // Injected by Spring
	@Mock
	private DiscountService discountService; // Injected by Spring
	@Mock
	private TypePayService typePayService; // Injected by Spring

	private CommandFactoryForTest commandFactoryForTest = new CommandFactoryForTest();
	private StatusFactoryForTest statusFactoryForTest = new StatusFactoryForTest();
	private UserFactoryForTest userFactoryForTest = new UserFactoryForTest();
	private DiscountFactoryForTest discountFactoryForTest = new DiscountFactoryForTest();
	private TypePayFactoryForTest typePayFactoryForTest = new TypePayFactoryForTest();

	List<Status> statuss = new ArrayList<Status>();
	List<User> users = new ArrayList<User>();
	List<Discount> discounts = new ArrayList<Discount>();
	List<TypePay> typePays = new ArrayList<TypePay>();

	private void givenPopulateModel() {
		Status status1 = statusFactoryForTest.newStatus();
		Status status2 = statusFactoryForTest.newStatus();
		List<Status> statuss = new ArrayList<Status>();
		statuss.add(status1);
		statuss.add(status2);
		when(statusService.findAll()).thenReturn(statuss);

		User user1 = userFactoryForTest.newUser();
		User user2 = userFactoryForTest.newUser();
		List<User> users = new ArrayList<User>();
		users.add(user1);
		users.add(user2);
		when(userService.findAll()).thenReturn(users);

		Discount discount1 = discountFactoryForTest.newDiscount();
		Discount discount2 = discountFactoryForTest.newDiscount();
		List<Discount> discounts = new ArrayList<Discount>();
		discounts.add(discount1);
		discounts.add(discount2);
		when(discountService.findAll()).thenReturn(discounts);

		TypePay typePay1 = typePayFactoryForTest.newTypePay();
		TypePay typePay2 = typePayFactoryForTest.newTypePay();
		List<TypePay> typePays = new ArrayList<TypePay>();
		typePays.add(typePay1);
		typePays.add(typePay2);
		when(typePayService.findAll()).thenReturn(typePays);

	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<Command> list = new ArrayList<Command>();
		when(commandService.findAll()).thenReturn(list);
		
		// When
		String viewName = commandController.list(model);
		
		// Then
		assertEquals("command/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = commandController.formForCreate(model);
		
		// Then
		assertEquals("command/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((Command)modelMap.get("command")).getId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/command/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<StatusListItem> statusListItems = (List<StatusListItem>) modelMap.get("listOfStatusItems");
		assertEquals(2, statusListItems.size());
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
		@SuppressWarnings("unchecked")
		List<DiscountListItem> discountListItems = (List<DiscountListItem>) modelMap.get("listOfDiscountItems");
		assertEquals(2, discountListItems.size());
		
		@SuppressWarnings("unchecked")
		List<TypePayListItem> typePayListItems = (List<TypePayListItem>) modelMap.get("listOfTypePayItems");
		assertEquals(2, typePayListItems.size());
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Command command = commandFactoryForTest.newCommand();
		Integer id = command.getId();
		when(commandService.findById(id)).thenReturn(command);
		
		// When
		String viewName = commandController.formForUpdate(model, id);
		
		// Then
		assertEquals("command/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(command, (Command) modelMap.get("command"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/command/update", modelMap.get("saveAction"));
		
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
		List<TypePayListItem> typePayListItems = (List<TypePayListItem>) modelMap.get("listOfTypePayItems");
		assertEquals(2, typePayListItems.size());
		
		List<DiscountListItem> discountListItems = (List<DiscountListItem>) modelMap.get("listOfDiscountItems");
		assertEquals(2, discountListItems.size());
		
		List<StatusListItem> statusListItems = (List<StatusListItem>) modelMap.get("listOfStatusItems");
		assertEquals(2, statusListItems.size());
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Command command = commandFactoryForTest.newCommand();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Command commandCreated = new Command();
		when(commandService.create(command)).thenReturn(commandCreated); 
		
		// When
		String viewName = commandController.create(command, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/command/form/"+command.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(commandCreated, (Command) modelMap.get("command"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Command command = commandFactoryForTest.newCommand();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = commandController.create(command, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("command/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(command, (Command) modelMap.get("command"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/command/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<StatusListItem> statusListItems = (List<StatusListItem>) modelMap.get("listOfStatusItems");
		assertEquals(2, statusListItems.size());
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
		@SuppressWarnings("unchecked")
		List<DiscountListItem> discountListItems = (List<DiscountListItem>) modelMap.get("listOfDiscountItems");
		assertEquals(2, discountListItems.size());
		
		@SuppressWarnings("unchecked")
		List<TypePayListItem> typePayListItems = (List<TypePayListItem>) modelMap.get("listOfTypePayItems");
		assertEquals(2, typePayListItems.size());
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Command command = commandFactoryForTest.newCommand();
		
		Exception exception = new RuntimeException("test exception");
		when(commandService.create(command)).thenThrow(exception);
		
		// When
		String viewName = commandController.create(command, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("command/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(command, (Command) modelMap.get("command"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/command/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "command.error.create", exception);
		
		@SuppressWarnings("unchecked")
		List<StatusListItem> statusListItems = (List<StatusListItem>) modelMap.get("listOfStatusItems");
		assertEquals(2, statusListItems.size());
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
		@SuppressWarnings("unchecked")
		List<DiscountListItem> discountListItems = (List<DiscountListItem>) modelMap.get("listOfDiscountItems");
		assertEquals(2, discountListItems.size());
		
		@SuppressWarnings("unchecked")
		List<TypePayListItem> typePayListItems = (List<TypePayListItem>) modelMap.get("listOfTypePayItems");
		assertEquals(2, typePayListItems.size());
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Command command = commandFactoryForTest.newCommand();
		Integer id = command.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Command commandSaved = new Command();
		commandSaved.setId(id);
		when(commandService.update(command)).thenReturn(commandSaved); 
		
		// When
		String viewName = commandController.update(command, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/command/form/"+command.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(commandSaved, (Command) modelMap.get("command"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Command command = commandFactoryForTest.newCommand();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = commandController.update(command, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("command/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(command, (Command) modelMap.get("command"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/command/update", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
		@SuppressWarnings("unchecked")
		List<TypePayListItem> typePayListItems = (List<TypePayListItem>) modelMap.get("listOfTypePayItems");
		assertEquals(2, typePayListItems.size());
		
		@SuppressWarnings("unchecked")
		List<DiscountListItem> discountListItems = (List<DiscountListItem>) modelMap.get("listOfDiscountItems");
		assertEquals(2, discountListItems.size());
		
		@SuppressWarnings("unchecked")
		List<StatusListItem> statusListItems = (List<StatusListItem>) modelMap.get("listOfStatusItems");
		assertEquals(2, statusListItems.size());
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Command command = commandFactoryForTest.newCommand();
		
		Exception exception = new RuntimeException("test exception");
		when(commandService.update(command)).thenThrow(exception);
		
		// When
		String viewName = commandController.update(command, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("command/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(command, (Command) modelMap.get("command"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/command/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "command.error.update", exception);
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
		@SuppressWarnings("unchecked")
		List<TypePayListItem> typePayListItems = (List<TypePayListItem>) modelMap.get("listOfTypePayItems");
		assertEquals(2, typePayListItems.size());
		
		@SuppressWarnings("unchecked")
		List<DiscountListItem> discountListItems = (List<DiscountListItem>) modelMap.get("listOfDiscountItems");
		assertEquals(2, discountListItems.size());
		
		@SuppressWarnings("unchecked")
		List<StatusListItem> statusListItems = (List<StatusListItem>) modelMap.get("listOfStatusItems");
		assertEquals(2, statusListItems.size());
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Command command = commandFactoryForTest.newCommand();
		Integer id = command.getId();
		
		// When
		String viewName = commandController.delete(redirectAttributes, id);
		
		// Then
		verify(commandService).delete(id);
		assertEquals("redirect:/command", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Command command = commandFactoryForTest.newCommand();
		Integer id = command.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(commandService).delete(id);
		
		// When
		String viewName = commandController.delete(redirectAttributes, id);
		
		// Then
		verify(commandService).delete(id);
		assertEquals("redirect:/command", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "command.error.delete", exception);
	}
	
	
}
