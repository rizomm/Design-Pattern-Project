package com.capsoagritrancommerce.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.capsoagritrancommerce.entity.Whishlist;
import com.capsoagritrancommerce.entity.User;
import com.capsoagritrancommerce.test.WhishlistFactoryForTest;
import com.capsoagritrancommerce.test.UserFactoryForTest;

//--- Services 
import com.capsoagritrancommerce.business.service.WhishlistService;
import com.capsoagritrancommerce.business.service.UserService;

//--- List Items 
import com.capsoagritrancommerce.web.listitem.UserListItem;

import com.capsoagritrancommerce.web.common.Message;
import com.capsoagritrancommerce.web.common.MessageHelper;
import com.capsoagritrancommerce.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class WhishlistControllerTest {
	
	@InjectMocks
	private WhishlistController whishlistController;
	@Mock
	private WhishlistService whishlistService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;
	@Mock
	private UserService userService; // Injected by Spring

	private WhishlistFactoryForTest whishlistFactoryForTest = new WhishlistFactoryForTest();
	private UserFactoryForTest userFactoryForTest = new UserFactoryForTest();

	List<User> users = new ArrayList<User>();

	private void givenPopulateModel() {
		User user1 = userFactoryForTest.newUser();
		User user2 = userFactoryForTest.newUser();
		List<User> users = new ArrayList<User>();
		users.add(user1);
		users.add(user2);
		when(userService.findAll()).thenReturn(users);

	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<Whishlist> list = new ArrayList<Whishlist>();
		when(whishlistService.findAll()).thenReturn(list);
		
		// When
		String viewName = whishlistController.list(model);
		
		// Then
		assertEquals("whishlist/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = whishlistController.formForCreate(model);
		
		// Then
		assertEquals("whishlist/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((Whishlist)modelMap.get("whishlist")).getId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/whishlist/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Whishlist whishlist = whishlistFactoryForTest.newWhishlist();
		Integer id = whishlist.getId();
		when(whishlistService.findById(id)).thenReturn(whishlist);
		
		// When
		String viewName = whishlistController.formForUpdate(model, id);
		
		// Then
		assertEquals("whishlist/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(whishlist, (Whishlist) modelMap.get("whishlist"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/whishlist/update", modelMap.get("saveAction"));
		
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Whishlist whishlist = whishlistFactoryForTest.newWhishlist();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Whishlist whishlistCreated = new Whishlist();
		when(whishlistService.create(whishlist)).thenReturn(whishlistCreated); 
		
		// When
		String viewName = whishlistController.create(whishlist, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/whishlist/form/"+whishlist.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(whishlistCreated, (Whishlist) modelMap.get("whishlist"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Whishlist whishlist = whishlistFactoryForTest.newWhishlist();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = whishlistController.create(whishlist, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("whishlist/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(whishlist, (Whishlist) modelMap.get("whishlist"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/whishlist/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Whishlist whishlist = whishlistFactoryForTest.newWhishlist();
		
		Exception exception = new RuntimeException("test exception");
		when(whishlistService.create(whishlist)).thenThrow(exception);
		
		// When
		String viewName = whishlistController.create(whishlist, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("whishlist/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(whishlist, (Whishlist) modelMap.get("whishlist"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/whishlist/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "whishlist.error.create", exception);
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Whishlist whishlist = whishlistFactoryForTest.newWhishlist();
		Integer id = whishlist.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Whishlist whishlistSaved = new Whishlist();
		whishlistSaved.setId(id);
		when(whishlistService.update(whishlist)).thenReturn(whishlistSaved); 
		
		// When
		String viewName = whishlistController.update(whishlist, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/whishlist/form/"+whishlist.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(whishlistSaved, (Whishlist) modelMap.get("whishlist"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Whishlist whishlist = whishlistFactoryForTest.newWhishlist();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = whishlistController.update(whishlist, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("whishlist/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(whishlist, (Whishlist) modelMap.get("whishlist"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/whishlist/update", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Whishlist whishlist = whishlistFactoryForTest.newWhishlist();
		
		Exception exception = new RuntimeException("test exception");
		when(whishlistService.update(whishlist)).thenThrow(exception);
		
		// When
		String viewName = whishlistController.update(whishlist, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("whishlist/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(whishlist, (Whishlist) modelMap.get("whishlist"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/whishlist/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "whishlist.error.update", exception);
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Whishlist whishlist = whishlistFactoryForTest.newWhishlist();
		Integer id = whishlist.getId();
		
		// When
		String viewName = whishlistController.delete(redirectAttributes, id);
		
		// Then
		verify(whishlistService).delete(id);
		assertEquals("redirect:/whishlist", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Whishlist whishlist = whishlistFactoryForTest.newWhishlist();
		Integer id = whishlist.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(whishlistService).delete(id);
		
		// When
		String viewName = whishlistController.delete(redirectAttributes, id);
		
		// Then
		verify(whishlistService).delete(id);
		assertEquals("redirect:/whishlist", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "whishlist.error.delete", exception);
	}
	
	
}
