package com.capsoagritrancommerce.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.capsoagritrancommerce.entity.Address;
import com.capsoagritrancommerce.entity.Country;
import com.capsoagritrancommerce.entity.User;
import com.capsoagritrancommerce.test.AddressFactoryForTest;
import com.capsoagritrancommerce.test.CountryFactoryForTest;
import com.capsoagritrancommerce.test.UserFactoryForTest;

//--- Services 
import com.capsoagritrancommerce.business.service.AddressService;
import com.capsoagritrancommerce.business.service.CountryService;
import com.capsoagritrancommerce.business.service.UserService;

//--- List Items 
import com.capsoagritrancommerce.web.listitem.CountryListItem;
import com.capsoagritrancommerce.web.listitem.UserListItem;

import com.capsoagritrancommerce.web.common.Message;
import com.capsoagritrancommerce.web.common.MessageHelper;
import com.capsoagritrancommerce.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class AddressControllerTest {
	
	@InjectMocks
	private AddressController addressController;
	@Mock
	private AddressService addressService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;
	@Mock
	private CountryService countryService; // Injected by Spring
	@Mock
	private UserService userService; // Injected by Spring

	private AddressFactoryForTest addressFactoryForTest = new AddressFactoryForTest();
	private CountryFactoryForTest countryFactoryForTest = new CountryFactoryForTest();
	private UserFactoryForTest userFactoryForTest = new UserFactoryForTest();

	List<Country> countrys = new ArrayList<Country>();
	List<User> users = new ArrayList<User>();

	private void givenPopulateModel() {
		Country country1 = countryFactoryForTest.newCountry();
		Country country2 = countryFactoryForTest.newCountry();
		List<Country> countrys = new ArrayList<Country>();
		countrys.add(country1);
		countrys.add(country2);
		when(countryService.findAll()).thenReturn(countrys);

		User user1 = userFactoryForTest.newUser();
		User user2 = userFactoryForTest.newUser();
		List<User> users = new ArrayList<User>();
		users.add(user1);
		users.add(user2);
		when(userService.findAll()).thenReturn(users);

	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<Address> list = new ArrayList<Address>();
		when(addressService.findAll()).thenReturn(list);
		
		// When
		String viewName = addressController.list(model);
		
		// Then
		assertEquals("address/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = addressController.formForCreate(model);
		
		// Then
		assertEquals("address/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((Address)modelMap.get("address")).getId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/address/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<CountryListItem> countryListItems = (List<CountryListItem>) modelMap.get("listOfCountryItems");
		assertEquals(2, countryListItems.size());
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Address address = addressFactoryForTest.newAddress();
		Integer id = address.getId();
		when(addressService.findById(id)).thenReturn(address);
		
		// When
		String viewName = addressController.formForUpdate(model, id);
		
		// Then
		assertEquals("address/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(address, (Address) modelMap.get("address"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/address/update", modelMap.get("saveAction"));
		
		List<CountryListItem> countryListItems = (List<CountryListItem>) modelMap.get("listOfCountryItems");
		assertEquals(2, countryListItems.size());
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Address address = addressFactoryForTest.newAddress();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Address addressCreated = new Address();
		when(addressService.create(address)).thenReturn(addressCreated); 
		
		// When
		String viewName = addressController.create(address, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/address/form/"+address.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(addressCreated, (Address) modelMap.get("address"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Address address = addressFactoryForTest.newAddress();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = addressController.create(address, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("address/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(address, (Address) modelMap.get("address"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/address/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<CountryListItem> countryListItems = (List<CountryListItem>) modelMap.get("listOfCountryItems");
		assertEquals(2, countryListItems.size());
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Address address = addressFactoryForTest.newAddress();
		
		Exception exception = new RuntimeException("test exception");
		when(addressService.create(address)).thenThrow(exception);
		
		// When
		String viewName = addressController.create(address, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("address/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(address, (Address) modelMap.get("address"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/address/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "address.error.create", exception);
		
		@SuppressWarnings("unchecked")
		List<CountryListItem> countryListItems = (List<CountryListItem>) modelMap.get("listOfCountryItems");
		assertEquals(2, countryListItems.size());
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Address address = addressFactoryForTest.newAddress();
		Integer id = address.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Address addressSaved = new Address();
		addressSaved.setId(id);
		when(addressService.update(address)).thenReturn(addressSaved); 
		
		// When
		String viewName = addressController.update(address, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/address/form/"+address.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(addressSaved, (Address) modelMap.get("address"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Address address = addressFactoryForTest.newAddress();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = addressController.update(address, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("address/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(address, (Address) modelMap.get("address"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/address/update", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<CountryListItem> countryListItems = (List<CountryListItem>) modelMap.get("listOfCountryItems");
		assertEquals(2, countryListItems.size());
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Address address = addressFactoryForTest.newAddress();
		
		Exception exception = new RuntimeException("test exception");
		when(addressService.update(address)).thenThrow(exception);
		
		// When
		String viewName = addressController.update(address, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("address/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(address, (Address) modelMap.get("address"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/address/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "address.error.update", exception);
		
		@SuppressWarnings("unchecked")
		List<CountryListItem> countryListItems = (List<CountryListItem>) modelMap.get("listOfCountryItems");
		assertEquals(2, countryListItems.size());
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Address address = addressFactoryForTest.newAddress();
		Integer id = address.getId();
		
		// When
		String viewName = addressController.delete(redirectAttributes, id);
		
		// Then
		verify(addressService).delete(id);
		assertEquals("redirect:/address", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Address address = addressFactoryForTest.newAddress();
		Integer id = address.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(addressService).delete(id);
		
		// When
		String viewName = addressController.delete(redirectAttributes, id);
		
		// Then
		verify(addressService).delete(id);
		assertEquals("redirect:/address", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "address.error.delete", exception);
	}
	
	
}
