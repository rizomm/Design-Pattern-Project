package com.capsoagritrancommerce.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.capsoagritrancommerce.entity.ProductHasRating;
import com.capsoagritrancommerce.test.ProductHasRatingFactoryForTest;

//--- Services 
import com.capsoagritrancommerce.business.service.ProductHasRatingService;


import com.capsoagritrancommerce.web.common.Message;
import com.capsoagritrancommerce.web.common.MessageHelper;
import com.capsoagritrancommerce.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class ProductHasRatingControllerTest {
	
	@InjectMocks
	private ProductHasRatingController productHasRatingController;
	@Mock
	private ProductHasRatingService productHasRatingService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;

	private ProductHasRatingFactoryForTest productHasRatingFactoryForTest = new ProductHasRatingFactoryForTest();


	private void givenPopulateModel() {
	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<ProductHasRating> list = new ArrayList<ProductHasRating>();
		when(productHasRatingService.findAll()).thenReturn(list);
		
		// When
		String viewName = productHasRatingController.list(model);
		
		// Then
		assertEquals("productHasRating/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = productHasRatingController.formForCreate(model);
		
		// Then
		assertEquals("productHasRating/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((ProductHasRating)modelMap.get("productHasRating")).getProductId());
		assertNull(((ProductHasRating)modelMap.get("productHasRating")).getRatingId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/productHasRating/create", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		ProductHasRating productHasRating = productHasRatingFactoryForTest.newProductHasRating();
		Integer productId = productHasRating.getProductId();
		Integer ratingId = productHasRating.getRatingId();
		when(productHasRatingService.findById(productId, ratingId)).thenReturn(productHasRating);
		
		// When
		String viewName = productHasRatingController.formForUpdate(model, productId, ratingId);
		
		// Then
		assertEquals("productHasRating/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(productHasRating, (ProductHasRating) modelMap.get("productHasRating"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/productHasRating/update", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		ProductHasRating productHasRating = productHasRatingFactoryForTest.newProductHasRating();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		ProductHasRating productHasRatingCreated = new ProductHasRating();
		when(productHasRatingService.create(productHasRating)).thenReturn(productHasRatingCreated); 
		
		// When
		String viewName = productHasRatingController.create(productHasRating, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/productHasRating/form/"+productHasRating.getProductId()+"/"+productHasRating.getRatingId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(productHasRatingCreated, (ProductHasRating) modelMap.get("productHasRating"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		ProductHasRating productHasRating = productHasRatingFactoryForTest.newProductHasRating();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = productHasRatingController.create(productHasRating, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("productHasRating/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(productHasRating, (ProductHasRating) modelMap.get("productHasRating"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/productHasRating/create", modelMap.get("saveAction"));
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		ProductHasRating productHasRating = productHasRatingFactoryForTest.newProductHasRating();
		
		Exception exception = new RuntimeException("test exception");
		when(productHasRatingService.create(productHasRating)).thenThrow(exception);
		
		// When
		String viewName = productHasRatingController.create(productHasRating, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("productHasRating/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(productHasRating, (ProductHasRating) modelMap.get("productHasRating"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/productHasRating/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "productHasRating.error.create", exception);
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		ProductHasRating productHasRating = productHasRatingFactoryForTest.newProductHasRating();
		Integer productId = productHasRating.getProductId();
		Integer ratingId = productHasRating.getRatingId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		ProductHasRating productHasRatingSaved = new ProductHasRating();
		productHasRatingSaved.setProductId(productId);
		productHasRatingSaved.setRatingId(ratingId);
		when(productHasRatingService.update(productHasRating)).thenReturn(productHasRatingSaved); 
		
		// When
		String viewName = productHasRatingController.update(productHasRating, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/productHasRating/form/"+productHasRating.getProductId()+"/"+productHasRating.getRatingId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(productHasRatingSaved, (ProductHasRating) modelMap.get("productHasRating"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		ProductHasRating productHasRating = productHasRatingFactoryForTest.newProductHasRating();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = productHasRatingController.update(productHasRating, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("productHasRating/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(productHasRating, (ProductHasRating) modelMap.get("productHasRating"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/productHasRating/update", modelMap.get("saveAction"));
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		ProductHasRating productHasRating = productHasRatingFactoryForTest.newProductHasRating();
		
		Exception exception = new RuntimeException("test exception");
		when(productHasRatingService.update(productHasRating)).thenThrow(exception);
		
		// When
		String viewName = productHasRatingController.update(productHasRating, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("productHasRating/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(productHasRating, (ProductHasRating) modelMap.get("productHasRating"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/productHasRating/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "productHasRating.error.update", exception);
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		ProductHasRating productHasRating = productHasRatingFactoryForTest.newProductHasRating();
		Integer productId = productHasRating.getProductId();
		Integer ratingId = productHasRating.getRatingId();
		
		// When
		String viewName = productHasRatingController.delete(redirectAttributes, productId, ratingId);
		
		// Then
		verify(productHasRatingService).delete(productId, ratingId);
		assertEquals("redirect:/productHasRating", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		ProductHasRating productHasRating = productHasRatingFactoryForTest.newProductHasRating();
		Integer productId = productHasRating.getProductId();
		Integer ratingId = productHasRating.getRatingId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(productHasRatingService).delete(productId, ratingId);
		
		// When
		String viewName = productHasRatingController.delete(redirectAttributes, productId, ratingId);
		
		// Then
		verify(productHasRatingService).delete(productId, ratingId);
		assertEquals("redirect:/productHasRating", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "productHasRating.error.delete", exception);
	}
	
	
}
