package com.capsoagritrancommerce.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.capsoagritrancommerce.entity.WhishlistHasProduct;
import com.capsoagritrancommerce.test.WhishlistHasProductFactoryForTest;

//--- Services 
import com.capsoagritrancommerce.business.service.WhishlistHasProductService;


import com.capsoagritrancommerce.web.common.Message;
import com.capsoagritrancommerce.web.common.MessageHelper;
import com.capsoagritrancommerce.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class WhishlistHasProductControllerTest {
	
	@InjectMocks
	private WhishlistHasProductController whishlistHasProductController;
	@Mock
	private WhishlistHasProductService whishlistHasProductService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;

	private WhishlistHasProductFactoryForTest whishlistHasProductFactoryForTest = new WhishlistHasProductFactoryForTest();


	private void givenPopulateModel() {
	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<WhishlistHasProduct> list = new ArrayList<WhishlistHasProduct>();
		when(whishlistHasProductService.findAll()).thenReturn(list);
		
		// When
		String viewName = whishlistHasProductController.list(model);
		
		// Then
		assertEquals("whishlistHasProduct/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = whishlistHasProductController.formForCreate(model);
		
		// Then
		assertEquals("whishlistHasProduct/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((WhishlistHasProduct)modelMap.get("whishlistHasProduct")).getWhishlistId());
		assertNull(((WhishlistHasProduct)modelMap.get("whishlistHasProduct")).getProductId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/whishlistHasProduct/create", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		WhishlistHasProduct whishlistHasProduct = whishlistHasProductFactoryForTest.newWhishlistHasProduct();
		Integer whishlistId = whishlistHasProduct.getWhishlistId();
		Integer productId = whishlistHasProduct.getProductId();
		when(whishlistHasProductService.findById(whishlistId, productId)).thenReturn(whishlistHasProduct);
		
		// When
		String viewName = whishlistHasProductController.formForUpdate(model, whishlistId, productId);
		
		// Then
		assertEquals("whishlistHasProduct/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(whishlistHasProduct, (WhishlistHasProduct) modelMap.get("whishlistHasProduct"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/whishlistHasProduct/update", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		WhishlistHasProduct whishlistHasProduct = whishlistHasProductFactoryForTest.newWhishlistHasProduct();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		WhishlistHasProduct whishlistHasProductCreated = new WhishlistHasProduct();
		when(whishlistHasProductService.create(whishlistHasProduct)).thenReturn(whishlistHasProductCreated); 
		
		// When
		String viewName = whishlistHasProductController.create(whishlistHasProduct, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/whishlistHasProduct/form/"+whishlistHasProduct.getWhishlistId()+"/"+whishlistHasProduct.getProductId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(whishlistHasProductCreated, (WhishlistHasProduct) modelMap.get("whishlistHasProduct"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		WhishlistHasProduct whishlistHasProduct = whishlistHasProductFactoryForTest.newWhishlistHasProduct();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = whishlistHasProductController.create(whishlistHasProduct, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("whishlistHasProduct/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(whishlistHasProduct, (WhishlistHasProduct) modelMap.get("whishlistHasProduct"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/whishlistHasProduct/create", modelMap.get("saveAction"));
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		WhishlistHasProduct whishlistHasProduct = whishlistHasProductFactoryForTest.newWhishlistHasProduct();
		
		Exception exception = new RuntimeException("test exception");
		when(whishlistHasProductService.create(whishlistHasProduct)).thenThrow(exception);
		
		// When
		String viewName = whishlistHasProductController.create(whishlistHasProduct, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("whishlistHasProduct/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(whishlistHasProduct, (WhishlistHasProduct) modelMap.get("whishlistHasProduct"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/whishlistHasProduct/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "whishlistHasProduct.error.create", exception);
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		WhishlistHasProduct whishlistHasProduct = whishlistHasProductFactoryForTest.newWhishlistHasProduct();
		Integer whishlistId = whishlistHasProduct.getWhishlistId();
		Integer productId = whishlistHasProduct.getProductId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		WhishlistHasProduct whishlistHasProductSaved = new WhishlistHasProduct();
		whishlistHasProductSaved.setWhishlistId(whishlistId);
		whishlistHasProductSaved.setProductId(productId);
		when(whishlistHasProductService.update(whishlistHasProduct)).thenReturn(whishlistHasProductSaved); 
		
		// When
		String viewName = whishlistHasProductController.update(whishlistHasProduct, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/whishlistHasProduct/form/"+whishlistHasProduct.getWhishlistId()+"/"+whishlistHasProduct.getProductId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(whishlistHasProductSaved, (WhishlistHasProduct) modelMap.get("whishlistHasProduct"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		WhishlistHasProduct whishlistHasProduct = whishlistHasProductFactoryForTest.newWhishlistHasProduct();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = whishlistHasProductController.update(whishlistHasProduct, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("whishlistHasProduct/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(whishlistHasProduct, (WhishlistHasProduct) modelMap.get("whishlistHasProduct"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/whishlistHasProduct/update", modelMap.get("saveAction"));
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		WhishlistHasProduct whishlistHasProduct = whishlistHasProductFactoryForTest.newWhishlistHasProduct();
		
		Exception exception = new RuntimeException("test exception");
		when(whishlistHasProductService.update(whishlistHasProduct)).thenThrow(exception);
		
		// When
		String viewName = whishlistHasProductController.update(whishlistHasProduct, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("whishlistHasProduct/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(whishlistHasProduct, (WhishlistHasProduct) modelMap.get("whishlistHasProduct"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/whishlistHasProduct/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "whishlistHasProduct.error.update", exception);
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		WhishlistHasProduct whishlistHasProduct = whishlistHasProductFactoryForTest.newWhishlistHasProduct();
		Integer whishlistId = whishlistHasProduct.getWhishlistId();
		Integer productId = whishlistHasProduct.getProductId();
		
		// When
		String viewName = whishlistHasProductController.delete(redirectAttributes, whishlistId, productId);
		
		// Then
		verify(whishlistHasProductService).delete(whishlistId, productId);
		assertEquals("redirect:/whishlistHasProduct", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		WhishlistHasProduct whishlistHasProduct = whishlistHasProductFactoryForTest.newWhishlistHasProduct();
		Integer whishlistId = whishlistHasProduct.getWhishlistId();
		Integer productId = whishlistHasProduct.getProductId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(whishlistHasProductService).delete(whishlistId, productId);
		
		// When
		String viewName = whishlistHasProductController.delete(redirectAttributes, whishlistId, productId);
		
		// Then
		verify(whishlistHasProductService).delete(whishlistId, productId);
		assertEquals("redirect:/whishlistHasProduct", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "whishlistHasProduct.error.delete", exception);
	}
	
	
}
