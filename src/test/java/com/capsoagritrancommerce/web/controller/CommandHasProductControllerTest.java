package com.capsoagritrancommerce.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.capsoagritrancommerce.entity.CommandHasProduct;
import com.capsoagritrancommerce.entity.Command;
import com.capsoagritrancommerce.entity.Product;
import com.capsoagritrancommerce.test.CommandHasProductFactoryForTest;
import com.capsoagritrancommerce.test.CommandFactoryForTest;
import com.capsoagritrancommerce.test.ProductFactoryForTest;

//--- Services 
import com.capsoagritrancommerce.business.service.CommandHasProductService;
import com.capsoagritrancommerce.business.service.CommandService;
import com.capsoagritrancommerce.business.service.ProductService;

//--- List Items 
import com.capsoagritrancommerce.web.listitem.CommandListItem;
import com.capsoagritrancommerce.web.listitem.ProductListItem;

import com.capsoagritrancommerce.web.common.Message;
import com.capsoagritrancommerce.web.common.MessageHelper;
import com.capsoagritrancommerce.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class CommandHasProductControllerTest {
	
	@InjectMocks
	private CommandHasProductController commandHasProductController;
	@Mock
	private CommandHasProductService commandHasProductService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;
	@Mock
	private CommandService commandService; // Injected by Spring
	@Mock
	private ProductService productService; // Injected by Spring

	private CommandHasProductFactoryForTest commandHasProductFactoryForTest = new CommandHasProductFactoryForTest();
	private CommandFactoryForTest commandFactoryForTest = new CommandFactoryForTest();
	private ProductFactoryForTest productFactoryForTest = new ProductFactoryForTest();

	List<Command> commands = new ArrayList<Command>();
	List<Product> products = new ArrayList<Product>();

	private void givenPopulateModel() {
		Command command1 = commandFactoryForTest.newCommand();
		Command command2 = commandFactoryForTest.newCommand();
		List<Command> commands = new ArrayList<Command>();
		commands.add(command1);
		commands.add(command2);
		when(commandService.findAll()).thenReturn(commands);

		Product product1 = productFactoryForTest.newProduct();
		Product product2 = productFactoryForTest.newProduct();
		List<Product> products = new ArrayList<Product>();
		products.add(product1);
		products.add(product2);
		when(productService.findAll()).thenReturn(products);

	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<CommandHasProduct> list = new ArrayList<CommandHasProduct>();
		when(commandHasProductService.findAll()).thenReturn(list);
		
		// When
		String viewName = commandHasProductController.list(model);
		
		// Then
		assertEquals("commandHasProduct/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = commandHasProductController.formForCreate(model);
		
		// Then
		assertEquals("commandHasProduct/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((CommandHasProduct)modelMap.get("commandHasProduct")).getCommandId());
		assertNull(((CommandHasProduct)modelMap.get("commandHasProduct")).getProductId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/commandHasProduct/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<CommandListItem> commandListItems = (List<CommandListItem>) modelMap.get("listOfCommandItems");
		assertEquals(2, commandListItems.size());
		
		@SuppressWarnings("unchecked")
		List<ProductListItem> productListItems = (List<ProductListItem>) modelMap.get("listOfProductItems");
		assertEquals(2, productListItems.size());
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		CommandHasProduct commandHasProduct = commandHasProductFactoryForTest.newCommandHasProduct();
		Integer commandId = commandHasProduct.getCommandId();
		Integer productId = commandHasProduct.getProductId();
		when(commandHasProductService.findById(commandId, productId)).thenReturn(commandHasProduct);
		
		// When
		String viewName = commandHasProductController.formForUpdate(model, commandId, productId);
		
		// Then
		assertEquals("commandHasProduct/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(commandHasProduct, (CommandHasProduct) modelMap.get("commandHasProduct"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/commandHasProduct/update", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		CommandHasProduct commandHasProduct = commandHasProductFactoryForTest.newCommandHasProduct();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		CommandHasProduct commandHasProductCreated = new CommandHasProduct();
		when(commandHasProductService.create(commandHasProduct)).thenReturn(commandHasProductCreated); 
		
		// When
		String viewName = commandHasProductController.create(commandHasProduct, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/commandHasProduct/form/"+commandHasProduct.getCommandId()+"/"+commandHasProduct.getProductId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(commandHasProductCreated, (CommandHasProduct) modelMap.get("commandHasProduct"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		CommandHasProduct commandHasProduct = commandHasProductFactoryForTest.newCommandHasProduct();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = commandHasProductController.create(commandHasProduct, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("commandHasProduct/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(commandHasProduct, (CommandHasProduct) modelMap.get("commandHasProduct"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/commandHasProduct/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<CommandListItem> commandListItems = (List<CommandListItem>) modelMap.get("listOfCommandItems");
		assertEquals(2, commandListItems.size());
		
		@SuppressWarnings("unchecked")
		List<ProductListItem> productListItems = (List<ProductListItem>) modelMap.get("listOfProductItems");
		assertEquals(2, productListItems.size());
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		CommandHasProduct commandHasProduct = commandHasProductFactoryForTest.newCommandHasProduct();
		
		Exception exception = new RuntimeException("test exception");
		when(commandHasProductService.create(commandHasProduct)).thenThrow(exception);
		
		// When
		String viewName = commandHasProductController.create(commandHasProduct, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("commandHasProduct/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(commandHasProduct, (CommandHasProduct) modelMap.get("commandHasProduct"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/commandHasProduct/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "commandHasProduct.error.create", exception);
		
		@SuppressWarnings("unchecked")
		List<CommandListItem> commandListItems = (List<CommandListItem>) modelMap.get("listOfCommandItems");
		assertEquals(2, commandListItems.size());
		
		@SuppressWarnings("unchecked")
		List<ProductListItem> productListItems = (List<ProductListItem>) modelMap.get("listOfProductItems");
		assertEquals(2, productListItems.size());
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		CommandHasProduct commandHasProduct = commandHasProductFactoryForTest.newCommandHasProduct();
		Integer commandId = commandHasProduct.getCommandId();
		Integer productId = commandHasProduct.getProductId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		CommandHasProduct commandHasProductSaved = new CommandHasProduct();
		commandHasProductSaved.setCommandId(commandId);
		commandHasProductSaved.setProductId(productId);
		when(commandHasProductService.update(commandHasProduct)).thenReturn(commandHasProductSaved); 
		
		// When
		String viewName = commandHasProductController.update(commandHasProduct, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/commandHasProduct/form/"+commandHasProduct.getCommandId()+"/"+commandHasProduct.getProductId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(commandHasProductSaved, (CommandHasProduct) modelMap.get("commandHasProduct"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		CommandHasProduct commandHasProduct = commandHasProductFactoryForTest.newCommandHasProduct();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = commandHasProductController.update(commandHasProduct, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("commandHasProduct/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(commandHasProduct, (CommandHasProduct) modelMap.get("commandHasProduct"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/commandHasProduct/update", modelMap.get("saveAction"));
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		CommandHasProduct commandHasProduct = commandHasProductFactoryForTest.newCommandHasProduct();
		
		Exception exception = new RuntimeException("test exception");
		when(commandHasProductService.update(commandHasProduct)).thenThrow(exception);
		
		// When
		String viewName = commandHasProductController.update(commandHasProduct, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("commandHasProduct/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(commandHasProduct, (CommandHasProduct) modelMap.get("commandHasProduct"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/commandHasProduct/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "commandHasProduct.error.update", exception);
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		CommandHasProduct commandHasProduct = commandHasProductFactoryForTest.newCommandHasProduct();
		Integer commandId = commandHasProduct.getCommandId();
		Integer productId = commandHasProduct.getProductId();
		
		// When
		String viewName = commandHasProductController.delete(redirectAttributes, commandId, productId);
		
		// Then
		verify(commandHasProductService).delete(commandId, productId);
		assertEquals("redirect:/commandHasProduct", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		CommandHasProduct commandHasProduct = commandHasProductFactoryForTest.newCommandHasProduct();
		Integer commandId = commandHasProduct.getCommandId();
		Integer productId = commandHasProduct.getProductId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(commandHasProductService).delete(commandId, productId);
		
		// When
		String viewName = commandHasProductController.delete(redirectAttributes, commandId, productId);
		
		// Then
		verify(commandHasProductService).delete(commandId, productId);
		assertEquals("redirect:/commandHasProduct", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "commandHasProduct.error.delete", exception);
	}
	
	
}
