package com.capsoagritrancommerce.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.capsoagritrancommerce.entity.Musictype;
import com.capsoagritrancommerce.test.MusictypeFactoryForTest;

//--- Services 
import com.capsoagritrancommerce.business.service.MusictypeService;


import com.capsoagritrancommerce.web.common.Message;
import com.capsoagritrancommerce.web.common.MessageHelper;
import com.capsoagritrancommerce.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class MusictypeControllerTest {
	
	@InjectMocks
	private MusictypeController musictypeController;
	@Mock
	private MusictypeService musictypeService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;

	private MusictypeFactoryForTest musictypeFactoryForTest = new MusictypeFactoryForTest();


	private void givenPopulateModel() {
	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<Musictype> list = new ArrayList<Musictype>();
		when(musictypeService.findAll()).thenReturn(list);
		
		// When
		String viewName = musictypeController.list(model);
		
		// Then
		assertEquals("musictype/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = musictypeController.formForCreate(model);
		
		// Then
		assertEquals("musictype/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((Musictype)modelMap.get("musictype")).getId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/musictype/create", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Musictype musictype = musictypeFactoryForTest.newMusictype();
		Integer id = musictype.getId();
		when(musictypeService.findById(id)).thenReturn(musictype);
		
		// When
		String viewName = musictypeController.formForUpdate(model, id);
		
		// Then
		assertEquals("musictype/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(musictype, (Musictype) modelMap.get("musictype"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/musictype/update", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Musictype musictype = musictypeFactoryForTest.newMusictype();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Musictype musictypeCreated = new Musictype();
		when(musictypeService.create(musictype)).thenReturn(musictypeCreated); 
		
		// When
		String viewName = musictypeController.create(musictype, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/musictype/form/"+musictype.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(musictypeCreated, (Musictype) modelMap.get("musictype"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Musictype musictype = musictypeFactoryForTest.newMusictype();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = musictypeController.create(musictype, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("musictype/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(musictype, (Musictype) modelMap.get("musictype"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/musictype/create", modelMap.get("saveAction"));
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Musictype musictype = musictypeFactoryForTest.newMusictype();
		
		Exception exception = new RuntimeException("test exception");
		when(musictypeService.create(musictype)).thenThrow(exception);
		
		// When
		String viewName = musictypeController.create(musictype, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("musictype/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(musictype, (Musictype) modelMap.get("musictype"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/musictype/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "musictype.error.create", exception);
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Musictype musictype = musictypeFactoryForTest.newMusictype();
		Integer id = musictype.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Musictype musictypeSaved = new Musictype();
		musictypeSaved.setId(id);
		when(musictypeService.update(musictype)).thenReturn(musictypeSaved); 
		
		// When
		String viewName = musictypeController.update(musictype, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/musictype/form/"+musictype.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(musictypeSaved, (Musictype) modelMap.get("musictype"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Musictype musictype = musictypeFactoryForTest.newMusictype();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = musictypeController.update(musictype, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("musictype/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(musictype, (Musictype) modelMap.get("musictype"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/musictype/update", modelMap.get("saveAction"));
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Musictype musictype = musictypeFactoryForTest.newMusictype();
		
		Exception exception = new RuntimeException("test exception");
		when(musictypeService.update(musictype)).thenThrow(exception);
		
		// When
		String viewName = musictypeController.update(musictype, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("musictype/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(musictype, (Musictype) modelMap.get("musictype"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/musictype/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "musictype.error.update", exception);
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Musictype musictype = musictypeFactoryForTest.newMusictype();
		Integer id = musictype.getId();
		
		// When
		String viewName = musictypeController.delete(redirectAttributes, id);
		
		// Then
		verify(musictypeService).delete(id);
		assertEquals("redirect:/musictype", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Musictype musictype = musictypeFactoryForTest.newMusictype();
		Integer id = musictype.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(musictypeService).delete(id);
		
		// When
		String viewName = musictypeController.delete(redirectAttributes, id);
		
		// Then
		verify(musictypeService).delete(id);
		assertEquals("redirect:/musictype", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "musictype.error.delete", exception);
	}
	
	
}
