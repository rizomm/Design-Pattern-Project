package com.capsoagritrancommerce.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.capsoagritrancommerce.entity.Productcategory;
import com.capsoagritrancommerce.test.ProductcategoryFactoryForTest;

//--- Services 
import com.capsoagritrancommerce.business.service.ProductcategoryService;


import com.capsoagritrancommerce.web.common.Message;
import com.capsoagritrancommerce.web.common.MessageHelper;
import com.capsoagritrancommerce.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class ProductcategoryControllerTest {
	
	@InjectMocks
	private ProductcategoryController productcategoryController;
	@Mock
	private ProductcategoryService productcategoryService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;

	private ProductcategoryFactoryForTest productcategoryFactoryForTest = new ProductcategoryFactoryForTest();


	private void givenPopulateModel() {
	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<Productcategory> list = new ArrayList<Productcategory>();
		when(productcategoryService.findAll()).thenReturn(list);
		
		// When
		String viewName = productcategoryController.list(model);
		
		// Then
		assertEquals("productcategory/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = productcategoryController.formForCreate(model);
		
		// Then
		assertEquals("productcategory/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((Productcategory)modelMap.get("productcategory")).getId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/productcategory/create", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Productcategory productcategory = productcategoryFactoryForTest.newProductcategory();
		Integer id = productcategory.getId();
		when(productcategoryService.findById(id)).thenReturn(productcategory);
		
		// When
		String viewName = productcategoryController.formForUpdate(model, id);
		
		// Then
		assertEquals("productcategory/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(productcategory, (Productcategory) modelMap.get("productcategory"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/productcategory/update", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Productcategory productcategory = productcategoryFactoryForTest.newProductcategory();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Productcategory productcategoryCreated = new Productcategory();
		when(productcategoryService.create(productcategory)).thenReturn(productcategoryCreated); 
		
		// When
		String viewName = productcategoryController.create(productcategory, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/productcategory/form/"+productcategory.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(productcategoryCreated, (Productcategory) modelMap.get("productcategory"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Productcategory productcategory = productcategoryFactoryForTest.newProductcategory();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = productcategoryController.create(productcategory, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("productcategory/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(productcategory, (Productcategory) modelMap.get("productcategory"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/productcategory/create", modelMap.get("saveAction"));
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Productcategory productcategory = productcategoryFactoryForTest.newProductcategory();
		
		Exception exception = new RuntimeException("test exception");
		when(productcategoryService.create(productcategory)).thenThrow(exception);
		
		// When
		String viewName = productcategoryController.create(productcategory, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("productcategory/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(productcategory, (Productcategory) modelMap.get("productcategory"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/productcategory/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "productcategory.error.create", exception);
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Productcategory productcategory = productcategoryFactoryForTest.newProductcategory();
		Integer id = productcategory.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Productcategory productcategorySaved = new Productcategory();
		productcategorySaved.setId(id);
		when(productcategoryService.update(productcategory)).thenReturn(productcategorySaved); 
		
		// When
		String viewName = productcategoryController.update(productcategory, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/productcategory/form/"+productcategory.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(productcategorySaved, (Productcategory) modelMap.get("productcategory"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Productcategory productcategory = productcategoryFactoryForTest.newProductcategory();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = productcategoryController.update(productcategory, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("productcategory/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(productcategory, (Productcategory) modelMap.get("productcategory"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/productcategory/update", modelMap.get("saveAction"));
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Productcategory productcategory = productcategoryFactoryForTest.newProductcategory();
		
		Exception exception = new RuntimeException("test exception");
		when(productcategoryService.update(productcategory)).thenThrow(exception);
		
		// When
		String viewName = productcategoryController.update(productcategory, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("productcategory/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(productcategory, (Productcategory) modelMap.get("productcategory"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/productcategory/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "productcategory.error.update", exception);
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Productcategory productcategory = productcategoryFactoryForTest.newProductcategory();
		Integer id = productcategory.getId();
		
		// When
		String viewName = productcategoryController.delete(redirectAttributes, id);
		
		// Then
		verify(productcategoryService).delete(id);
		assertEquals("redirect:/productcategory", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Productcategory productcategory = productcategoryFactoryForTest.newProductcategory();
		Integer id = productcategory.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(productcategoryService).delete(id);
		
		// When
		String viewName = productcategoryController.delete(redirectAttributes, id);
		
		// Then
		verify(productcategoryService).delete(id);
		assertEquals("redirect:/productcategory", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "productcategory.error.delete", exception);
	}
	
	
}
