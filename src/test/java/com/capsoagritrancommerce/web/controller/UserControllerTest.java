package com.capsoagritrancommerce.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.capsoagritrancommerce.entity.User;
import com.capsoagritrancommerce.entity.Country;
import com.capsoagritrancommerce.test.UserFactoryForTest;
import com.capsoagritrancommerce.test.CountryFactoryForTest;

//--- Services 
import com.capsoagritrancommerce.business.service.UserService;
import com.capsoagritrancommerce.business.service.CountryService;

//--- List Items 
import com.capsoagritrancommerce.web.listitem.CountryListItem;

import com.capsoagritrancommerce.web.common.Message;
import com.capsoagritrancommerce.web.common.MessageHelper;
import com.capsoagritrancommerce.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {
	
	@InjectMocks
	private UserController userController;
	@Mock
	private UserService userService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;
	@Mock
	private CountryService countryService; // Injected by Spring

	private UserFactoryForTest userFactoryForTest = new UserFactoryForTest();
	private CountryFactoryForTest countryFactoryForTest = new CountryFactoryForTest();

	List<Country> countrys = new ArrayList<Country>();

	private void givenPopulateModel() {
		Country country1 = countryFactoryForTest.newCountry();
		Country country2 = countryFactoryForTest.newCountry();
		List<Country> countrys = new ArrayList<Country>();
		countrys.add(country1);
		countrys.add(country2);
		when(countryService.findAll()).thenReturn(countrys);

	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<User> list = new ArrayList<User>();
		when(userService.findAll()).thenReturn(list);
		
		// When
		String viewName = userController.list(model);
		
		// Then
		assertEquals("user/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = userController.formForCreate(model);
		
		// Then
		assertEquals("user/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((User)modelMap.get("user")).getId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/user/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<CountryListItem> countryListItems = (List<CountryListItem>) modelMap.get("listOfCountryItems");
		assertEquals(2, countryListItems.size());
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		User user = userFactoryForTest.newUser();
		Integer id = user.getId();
		when(userService.findById(id)).thenReturn(user);
		
		// When
		String viewName = userController.formForUpdate(model, id);
		
		// Then
		assertEquals("user/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(user, (User) modelMap.get("user"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/user/update", modelMap.get("saveAction"));
		
		List<CountryListItem> countryListItems = (List<CountryListItem>) modelMap.get("listOfCountryItems");
		assertEquals(2, countryListItems.size());
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		User user = userFactoryForTest.newUser();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		User userCreated = new User();
		when(userService.create(user)).thenReturn(userCreated); 
		
		// When
		String viewName = userController.create(user, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/user/form/"+user.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userCreated, (User) modelMap.get("user"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		User user = userFactoryForTest.newUser();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = userController.create(user, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("user/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(user, (User) modelMap.get("user"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/user/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<CountryListItem> countryListItems = (List<CountryListItem>) modelMap.get("listOfCountryItems");
		assertEquals(2, countryListItems.size());
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		User user = userFactoryForTest.newUser();
		
		Exception exception = new RuntimeException("test exception");
		when(userService.create(user)).thenThrow(exception);
		
		// When
		String viewName = userController.create(user, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("user/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(user, (User) modelMap.get("user"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/user/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "user.error.create", exception);
		
		@SuppressWarnings("unchecked")
		List<CountryListItem> countryListItems = (List<CountryListItem>) modelMap.get("listOfCountryItems");
		assertEquals(2, countryListItems.size());
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		User user = userFactoryForTest.newUser();
		Integer id = user.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		User userSaved = new User();
		userSaved.setId(id);
		when(userService.update(user)).thenReturn(userSaved); 
		
		// When
		String viewName = userController.update(user, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/user/form/"+user.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userSaved, (User) modelMap.get("user"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		User user = userFactoryForTest.newUser();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = userController.update(user, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("user/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(user, (User) modelMap.get("user"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/user/update", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<CountryListItem> countryListItems = (List<CountryListItem>) modelMap.get("listOfCountryItems");
		assertEquals(2, countryListItems.size());
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		User user = userFactoryForTest.newUser();
		
		Exception exception = new RuntimeException("test exception");
		when(userService.update(user)).thenThrow(exception);
		
		// When
		String viewName = userController.update(user, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("user/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(user, (User) modelMap.get("user"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/user/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "user.error.update", exception);
		
		@SuppressWarnings("unchecked")
		List<CountryListItem> countryListItems = (List<CountryListItem>) modelMap.get("listOfCountryItems");
		assertEquals(2, countryListItems.size());
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		User user = userFactoryForTest.newUser();
		Integer id = user.getId();
		
		// When
		String viewName = userController.delete(redirectAttributes, id);
		
		// Then
		verify(userService).delete(id);
		assertEquals("redirect:/user", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		User user = userFactoryForTest.newUser();
		Integer id = user.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(userService).delete(id);
		
		// When
		String viewName = userController.delete(redirectAttributes, id);
		
		// Then
		verify(userService).delete(id);
		assertEquals("redirect:/user", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "user.error.delete", exception);
	}
	
	
}
