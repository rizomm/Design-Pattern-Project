package com.capsoagritrancommerce.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.capsoagritrancommerce.entity.Basket;
import com.capsoagritrancommerce.entity.User;
import com.capsoagritrancommerce.entity.Product;
import com.capsoagritrancommerce.test.BasketFactoryForTest;
import com.capsoagritrancommerce.test.UserFactoryForTest;
import com.capsoagritrancommerce.test.ProductFactoryForTest;

//--- Services 
import com.capsoagritrancommerce.business.service.BasketService;
import com.capsoagritrancommerce.business.service.UserService;
import com.capsoagritrancommerce.business.service.ProductService;

//--- List Items 
import com.capsoagritrancommerce.web.listitem.UserListItem;
import com.capsoagritrancommerce.web.listitem.ProductListItem;

import com.capsoagritrancommerce.web.common.Message;
import com.capsoagritrancommerce.web.common.MessageHelper;
import com.capsoagritrancommerce.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class BasketControllerTest {
	
	@InjectMocks
	private BasketController basketController;
	@Mock
	private BasketService basketService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;
	@Mock
	private UserService userService; // Injected by Spring
	@Mock
	private ProductService productService; // Injected by Spring

	private BasketFactoryForTest basketFactoryForTest = new BasketFactoryForTest();
	private UserFactoryForTest userFactoryForTest = new UserFactoryForTest();
	private ProductFactoryForTest productFactoryForTest = new ProductFactoryForTest();

	List<User> users = new ArrayList<User>();
	List<Product> products = new ArrayList<Product>();

	private void givenPopulateModel() {
		User user1 = userFactoryForTest.newUser();
		User user2 = userFactoryForTest.newUser();
		List<User> users = new ArrayList<User>();
		users.add(user1);
		users.add(user2);
		when(userService.findAll()).thenReturn(users);

		Product product1 = productFactoryForTest.newProduct();
		Product product2 = productFactoryForTest.newProduct();
		List<Product> products = new ArrayList<Product>();
		products.add(product1);
		products.add(product2);
		when(productService.findAll()).thenReturn(products);

	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<Basket> list = new ArrayList<Basket>();
		when(basketService.findAll()).thenReturn(list);
		
		// When
		String viewName = basketController.list(model);
		
		// Then
		assertEquals("basket/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = basketController.formForCreate(model);
		
		// Then
		assertEquals("basket/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((Basket)modelMap.get("basket")).getId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/basket/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
		@SuppressWarnings("unchecked")
		List<ProductListItem> productListItems = (List<ProductListItem>) modelMap.get("listOfProductItems");
		assertEquals(2, productListItems.size());
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Basket basket = basketFactoryForTest.newBasket();
		Integer id = basket.getId();
		when(basketService.findById(id)).thenReturn(basket);
		
		// When
		String viewName = basketController.formForUpdate(model, id);
		
		// Then
		assertEquals("basket/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(basket, (Basket) modelMap.get("basket"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/basket/update", modelMap.get("saveAction"));
		
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Basket basket = basketFactoryForTest.newBasket();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Basket basketCreated = new Basket();
		when(basketService.create(basket)).thenReturn(basketCreated); 
		
		// When
		String viewName = basketController.create(basket, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/basket/form/"+basket.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(basketCreated, (Basket) modelMap.get("basket"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Basket basket = basketFactoryForTest.newBasket();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = basketController.create(basket, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("basket/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(basket, (Basket) modelMap.get("basket"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/basket/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
		@SuppressWarnings("unchecked")
		List<ProductListItem> productListItems = (List<ProductListItem>) modelMap.get("listOfProductItems");
		assertEquals(2, productListItems.size());
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Basket basket = basketFactoryForTest.newBasket();
		
		Exception exception = new RuntimeException("test exception");
		when(basketService.create(basket)).thenThrow(exception);
		
		// When
		String viewName = basketController.create(basket, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("basket/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(basket, (Basket) modelMap.get("basket"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/basket/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "basket.error.create", exception);
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
		@SuppressWarnings("unchecked")
		List<ProductListItem> productListItems = (List<ProductListItem>) modelMap.get("listOfProductItems");
		assertEquals(2, productListItems.size());
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Basket basket = basketFactoryForTest.newBasket();
		Integer id = basket.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Basket basketSaved = new Basket();
		basketSaved.setId(id);
		when(basketService.update(basket)).thenReturn(basketSaved); 
		
		// When
		String viewName = basketController.update(basket, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/basket/form/"+basket.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(basketSaved, (Basket) modelMap.get("basket"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Basket basket = basketFactoryForTest.newBasket();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = basketController.update(basket, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("basket/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(basket, (Basket) modelMap.get("basket"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/basket/update", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Basket basket = basketFactoryForTest.newBasket();
		
		Exception exception = new RuntimeException("test exception");
		when(basketService.update(basket)).thenThrow(exception);
		
		// When
		String viewName = basketController.update(basket, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("basket/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(basket, (Basket) modelMap.get("basket"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/basket/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "basket.error.update", exception);
		
		@SuppressWarnings("unchecked")
		List<UserListItem> userListItems = (List<UserListItem>) modelMap.get("listOfUserItems");
		assertEquals(2, userListItems.size());
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Basket basket = basketFactoryForTest.newBasket();
		Integer id = basket.getId();
		
		// When
		String viewName = basketController.delete(redirectAttributes, id);
		
		// Then
		verify(basketService).delete(id);
		assertEquals("redirect:/basket", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Basket basket = basketFactoryForTest.newBasket();
		Integer id = basket.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(basketService).delete(id);
		
		// When
		String viewName = basketController.delete(redirectAttributes, id);
		
		// Then
		verify(basketService).delete(id);
		assertEquals("redirect:/basket", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "basket.error.delete", exception);
	}
	
	
}
