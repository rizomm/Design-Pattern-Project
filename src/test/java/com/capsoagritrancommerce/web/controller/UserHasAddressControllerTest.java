package com.capsoagritrancommerce.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.capsoagritrancommerce.entity.UserHasAddress;
import com.capsoagritrancommerce.test.UserHasAddressFactoryForTest;

//--- Services 
import com.capsoagritrancommerce.business.service.UserHasAddressService;


import com.capsoagritrancommerce.web.common.Message;
import com.capsoagritrancommerce.web.common.MessageHelper;
import com.capsoagritrancommerce.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class UserHasAddressControllerTest {
	
	@InjectMocks
	private UserHasAddressController userHasAddressController;
	@Mock
	private UserHasAddressService userHasAddressService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;

	private UserHasAddressFactoryForTest userHasAddressFactoryForTest = new UserHasAddressFactoryForTest();


	private void givenPopulateModel() {
	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<UserHasAddress> list = new ArrayList<UserHasAddress>();
		when(userHasAddressService.findAll()).thenReturn(list);
		
		// When
		String viewName = userHasAddressController.list(model);
		
		// Then
		assertEquals("userHasAddress/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = userHasAddressController.formForCreate(model);
		
		// Then
		assertEquals("userHasAddress/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((UserHasAddress)modelMap.get("userHasAddress")).getUserId());
		assertNull(((UserHasAddress)modelMap.get("userHasAddress")).getAddressId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/userHasAddress/create", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		UserHasAddress userHasAddress = userHasAddressFactoryForTest.newUserHasAddress();
		Integer userId = userHasAddress.getUserId();
		Integer addressId = userHasAddress.getAddressId();
		when(userHasAddressService.findById(userId, addressId)).thenReturn(userHasAddress);
		
		// When
		String viewName = userHasAddressController.formForUpdate(model, userId, addressId);
		
		// Then
		assertEquals("userHasAddress/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasAddress, (UserHasAddress) modelMap.get("userHasAddress"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/userHasAddress/update", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		UserHasAddress userHasAddress = userHasAddressFactoryForTest.newUserHasAddress();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		UserHasAddress userHasAddressCreated = new UserHasAddress();
		when(userHasAddressService.create(userHasAddress)).thenReturn(userHasAddressCreated); 
		
		// When
		String viewName = userHasAddressController.create(userHasAddress, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/userHasAddress/form/"+userHasAddress.getUserId()+"/"+userHasAddress.getAddressId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasAddressCreated, (UserHasAddress) modelMap.get("userHasAddress"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		UserHasAddress userHasAddress = userHasAddressFactoryForTest.newUserHasAddress();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = userHasAddressController.create(userHasAddress, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("userHasAddress/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasAddress, (UserHasAddress) modelMap.get("userHasAddress"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/userHasAddress/create", modelMap.get("saveAction"));
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		UserHasAddress userHasAddress = userHasAddressFactoryForTest.newUserHasAddress();
		
		Exception exception = new RuntimeException("test exception");
		when(userHasAddressService.create(userHasAddress)).thenThrow(exception);
		
		// When
		String viewName = userHasAddressController.create(userHasAddress, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("userHasAddress/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasAddress, (UserHasAddress) modelMap.get("userHasAddress"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/userHasAddress/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "userHasAddress.error.create", exception);
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		UserHasAddress userHasAddress = userHasAddressFactoryForTest.newUserHasAddress();
		Integer userId = userHasAddress.getUserId();
		Integer addressId = userHasAddress.getAddressId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		UserHasAddress userHasAddressSaved = new UserHasAddress();
		userHasAddressSaved.setUserId(userId);
		userHasAddressSaved.setAddressId(addressId);
		when(userHasAddressService.update(userHasAddress)).thenReturn(userHasAddressSaved); 
		
		// When
		String viewName = userHasAddressController.update(userHasAddress, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/userHasAddress/form/"+userHasAddress.getUserId()+"/"+userHasAddress.getAddressId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasAddressSaved, (UserHasAddress) modelMap.get("userHasAddress"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		UserHasAddress userHasAddress = userHasAddressFactoryForTest.newUserHasAddress();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = userHasAddressController.update(userHasAddress, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("userHasAddress/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasAddress, (UserHasAddress) modelMap.get("userHasAddress"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/userHasAddress/update", modelMap.get("saveAction"));
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		UserHasAddress userHasAddress = userHasAddressFactoryForTest.newUserHasAddress();
		
		Exception exception = new RuntimeException("test exception");
		when(userHasAddressService.update(userHasAddress)).thenThrow(exception);
		
		// When
		String viewName = userHasAddressController.update(userHasAddress, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("userHasAddress/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(userHasAddress, (UserHasAddress) modelMap.get("userHasAddress"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/userHasAddress/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "userHasAddress.error.update", exception);
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		UserHasAddress userHasAddress = userHasAddressFactoryForTest.newUserHasAddress();
		Integer userId = userHasAddress.getUserId();
		Integer addressId = userHasAddress.getAddressId();
		
		// When
		String viewName = userHasAddressController.delete(redirectAttributes, userId, addressId);
		
		// Then
		verify(userHasAddressService).delete(userId, addressId);
		assertEquals("redirect:/userHasAddress", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		UserHasAddress userHasAddress = userHasAddressFactoryForTest.newUserHasAddress();
		Integer userId = userHasAddress.getUserId();
		Integer addressId = userHasAddress.getAddressId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(userHasAddressService).delete(userId, addressId);
		
		// When
		String viewName = userHasAddressController.delete(redirectAttributes, userId, addressId);
		
		// Then
		verify(userHasAddressService).delete(userId, addressId);
		assertEquals("redirect:/userHasAddress", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "userHasAddress.error.delete", exception);
	}
	
	
}
