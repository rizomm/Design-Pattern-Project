package com.capsoagritrancommerce.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.capsoagritrancommerce.entity.BasketHasProduct;
import com.capsoagritrancommerce.test.BasketHasProductFactoryForTest;

//--- Services 
import com.capsoagritrancommerce.business.service.BasketHasProductService;


import com.capsoagritrancommerce.web.common.Message;
import com.capsoagritrancommerce.web.common.MessageHelper;
import com.capsoagritrancommerce.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class BasketHasProductControllerTest {
	
	@InjectMocks
	private BasketHasProductController basketHasProductController;
	@Mock
	private BasketHasProductService basketHasProductService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;

	private BasketHasProductFactoryForTest basketHasProductFactoryForTest = new BasketHasProductFactoryForTest();


	private void givenPopulateModel() {
	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<BasketHasProduct> list = new ArrayList<BasketHasProduct>();
		when(basketHasProductService.findAll()).thenReturn(list);
		
		// When
		String viewName = basketHasProductController.list(model);
		
		// Then
		assertEquals("basketHasProduct/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = basketHasProductController.formForCreate(model);
		
		// Then
		assertEquals("basketHasProduct/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((BasketHasProduct)modelMap.get("basketHasProduct")).getBasketId());
		assertNull(((BasketHasProduct)modelMap.get("basketHasProduct")).getProductId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/basketHasProduct/create", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		BasketHasProduct basketHasProduct = basketHasProductFactoryForTest.newBasketHasProduct();
		Integer basketId = basketHasProduct.getBasketId();
		Integer productId = basketHasProduct.getProductId();
		when(basketHasProductService.findById(basketId, productId)).thenReturn(basketHasProduct);
		
		// When
		String viewName = basketHasProductController.formForUpdate(model, basketId, productId);
		
		// Then
		assertEquals("basketHasProduct/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(basketHasProduct, (BasketHasProduct) modelMap.get("basketHasProduct"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/basketHasProduct/update", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		BasketHasProduct basketHasProduct = basketHasProductFactoryForTest.newBasketHasProduct();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		BasketHasProduct basketHasProductCreated = new BasketHasProduct();
		when(basketHasProductService.create(basketHasProduct)).thenReturn(basketHasProductCreated); 
		
		// When
		String viewName = basketHasProductController.create(basketHasProduct, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/basketHasProduct/form/"+basketHasProduct.getBasketId()+"/"+basketHasProduct.getProductId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(basketHasProductCreated, (BasketHasProduct) modelMap.get("basketHasProduct"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		BasketHasProduct basketHasProduct = basketHasProductFactoryForTest.newBasketHasProduct();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = basketHasProductController.create(basketHasProduct, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("basketHasProduct/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(basketHasProduct, (BasketHasProduct) modelMap.get("basketHasProduct"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/basketHasProduct/create", modelMap.get("saveAction"));
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		BasketHasProduct basketHasProduct = basketHasProductFactoryForTest.newBasketHasProduct();
		
		Exception exception = new RuntimeException("test exception");
		when(basketHasProductService.create(basketHasProduct)).thenThrow(exception);
		
		// When
		String viewName = basketHasProductController.create(basketHasProduct, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("basketHasProduct/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(basketHasProduct, (BasketHasProduct) modelMap.get("basketHasProduct"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/basketHasProduct/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "basketHasProduct.error.create", exception);
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		BasketHasProduct basketHasProduct = basketHasProductFactoryForTest.newBasketHasProduct();
		Integer basketId = basketHasProduct.getBasketId();
		Integer productId = basketHasProduct.getProductId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		BasketHasProduct basketHasProductSaved = new BasketHasProduct();
		basketHasProductSaved.setBasketId(basketId);
		basketHasProductSaved.setProductId(productId);
		when(basketHasProductService.update(basketHasProduct)).thenReturn(basketHasProductSaved); 
		
		// When
		String viewName = basketHasProductController.update(basketHasProduct, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/basketHasProduct/form/"+basketHasProduct.getBasketId()+"/"+basketHasProduct.getProductId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(basketHasProductSaved, (BasketHasProduct) modelMap.get("basketHasProduct"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		BasketHasProduct basketHasProduct = basketHasProductFactoryForTest.newBasketHasProduct();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = basketHasProductController.update(basketHasProduct, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("basketHasProduct/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(basketHasProduct, (BasketHasProduct) modelMap.get("basketHasProduct"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/basketHasProduct/update", modelMap.get("saveAction"));
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		BasketHasProduct basketHasProduct = basketHasProductFactoryForTest.newBasketHasProduct();
		
		Exception exception = new RuntimeException("test exception");
		when(basketHasProductService.update(basketHasProduct)).thenThrow(exception);
		
		// When
		String viewName = basketHasProductController.update(basketHasProduct, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("basketHasProduct/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(basketHasProduct, (BasketHasProduct) modelMap.get("basketHasProduct"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/basketHasProduct/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "basketHasProduct.error.update", exception);
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		BasketHasProduct basketHasProduct = basketHasProductFactoryForTest.newBasketHasProduct();
		Integer basketId = basketHasProduct.getBasketId();
		Integer productId = basketHasProduct.getProductId();
		
		// When
		String viewName = basketHasProductController.delete(redirectAttributes, basketId, productId);
		
		// Then
		verify(basketHasProductService).delete(basketId, productId);
		assertEquals("redirect:/basketHasProduct", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		BasketHasProduct basketHasProduct = basketHasProductFactoryForTest.newBasketHasProduct();
		Integer basketId = basketHasProduct.getBasketId();
		Integer productId = basketHasProduct.getProductId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(basketHasProductService).delete(basketId, productId);
		
		// When
		String viewName = basketHasProductController.delete(redirectAttributes, basketId, productId);
		
		// Then
		verify(basketHasProductService).delete(basketId, productId);
		assertEquals("redirect:/basketHasProduct", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "basketHasProduct.error.delete", exception);
	}
	
	
}
