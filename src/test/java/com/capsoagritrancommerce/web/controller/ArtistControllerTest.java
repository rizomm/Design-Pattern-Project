package com.capsoagritrancommerce.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.capsoagritrancommerce.entity.Artist;
import com.capsoagritrancommerce.test.ArtistFactoryForTest;

//--- Services 
import com.capsoagritrancommerce.business.service.ArtistService;


import com.capsoagritrancommerce.web.common.Message;
import com.capsoagritrancommerce.web.common.MessageHelper;
import com.capsoagritrancommerce.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class ArtistControllerTest {
	
	@InjectMocks
	private ArtistController artistController;
	@Mock
	private ArtistService artistService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;

	private ArtistFactoryForTest artistFactoryForTest = new ArtistFactoryForTest();


	private void givenPopulateModel() {
	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<Artist> list = new ArrayList<Artist>();
		when(artistService.findAll()).thenReturn(list);
		
		// When
		String viewName = artistController.list(model);
		
		// Then
		assertEquals("artist/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = artistController.formForCreate(model);
		
		// Then
		assertEquals("artist/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((Artist)modelMap.get("artist")).getId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/artist/create", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Artist artist = artistFactoryForTest.newArtist();
		Integer id = artist.getId();
		when(artistService.findById(id)).thenReturn(artist);
		
		// When
		String viewName = artistController.formForUpdate(model, id);
		
		// Then
		assertEquals("artist/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(artist, (Artist) modelMap.get("artist"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/artist/update", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Artist artist = artistFactoryForTest.newArtist();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Artist artistCreated = new Artist();
		when(artistService.create(artist)).thenReturn(artistCreated); 
		
		// When
		String viewName = artistController.create(artist, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/artist/form/"+artist.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(artistCreated, (Artist) modelMap.get("artist"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Artist artist = artistFactoryForTest.newArtist();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = artistController.create(artist, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("artist/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(artist, (Artist) modelMap.get("artist"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/artist/create", modelMap.get("saveAction"));
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Artist artist = artistFactoryForTest.newArtist();
		
		Exception exception = new RuntimeException("test exception");
		when(artistService.create(artist)).thenThrow(exception);
		
		// When
		String viewName = artistController.create(artist, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("artist/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(artist, (Artist) modelMap.get("artist"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/artist/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "artist.error.create", exception);
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Artist artist = artistFactoryForTest.newArtist();
		Integer id = artist.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Artist artistSaved = new Artist();
		artistSaved.setId(id);
		when(artistService.update(artist)).thenReturn(artistSaved); 
		
		// When
		String viewName = artistController.update(artist, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/artist/form/"+artist.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(artistSaved, (Artist) modelMap.get("artist"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Artist artist = artistFactoryForTest.newArtist();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = artistController.update(artist, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("artist/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(artist, (Artist) modelMap.get("artist"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/artist/update", modelMap.get("saveAction"));
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Artist artist = artistFactoryForTest.newArtist();
		
		Exception exception = new RuntimeException("test exception");
		when(artistService.update(artist)).thenThrow(exception);
		
		// When
		String viewName = artistController.update(artist, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("artist/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(artist, (Artist) modelMap.get("artist"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/artist/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "artist.error.update", exception);
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Artist artist = artistFactoryForTest.newArtist();
		Integer id = artist.getId();
		
		// When
		String viewName = artistController.delete(redirectAttributes, id);
		
		// Then
		verify(artistService).delete(id);
		assertEquals("redirect:/artist", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Artist artist = artistFactoryForTest.newArtist();
		Integer id = artist.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(artistService).delete(id);
		
		// When
		String viewName = artistController.delete(redirectAttributes, id);
		
		// Then
		verify(artistService).delete(id);
		assertEquals("redirect:/artist", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "artist.error.delete", exception);
	}
	
	
}
