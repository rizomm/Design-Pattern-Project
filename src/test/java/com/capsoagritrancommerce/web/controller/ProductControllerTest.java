package com.capsoagritrancommerce.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.capsoagritrancommerce.entity.Product;
import com.capsoagritrancommerce.entity.Musictype;
import com.capsoagritrancommerce.entity.Artist;
import com.capsoagritrancommerce.entity.Productcategory;
import com.capsoagritrancommerce.entity.Rating;
import com.capsoagritrancommerce.entity.Whishlist;
import com.capsoagritrancommerce.test.ProductFactoryForTest;
import com.capsoagritrancommerce.test.MusictypeFactoryForTest;
import com.capsoagritrancommerce.test.ArtistFactoryForTest;
import com.capsoagritrancommerce.test.ProductcategoryFactoryForTest;
import com.capsoagritrancommerce.test.RatingFactoryForTest;
import com.capsoagritrancommerce.test.WhishlistFactoryForTest;

//--- Services 
import com.capsoagritrancommerce.business.service.ProductService;
import com.capsoagritrancommerce.business.service.MusictypeService;
import com.capsoagritrancommerce.business.service.ArtistService;
import com.capsoagritrancommerce.business.service.ProductcategoryService;
import com.capsoagritrancommerce.business.service.RatingService;
import com.capsoagritrancommerce.business.service.WhishlistService;

//--- List Items 
import com.capsoagritrancommerce.web.listitem.MusictypeListItem;
import com.capsoagritrancommerce.web.listitem.ArtistListItem;
import com.capsoagritrancommerce.web.listitem.ProductcategoryListItem;
import com.capsoagritrancommerce.web.listitem.RatingListItem;
import com.capsoagritrancommerce.web.listitem.WhishlistListItem;

import com.capsoagritrancommerce.web.common.Message;
import com.capsoagritrancommerce.web.common.MessageHelper;
import com.capsoagritrancommerce.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class ProductControllerTest {
	
	@InjectMocks
	private ProductController productController;
	@Mock
	private ProductService productService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;
	@Mock
	private MusictypeService musictypeService; // Injected by Spring
	@Mock
	private ArtistService artistService; // Injected by Spring
	@Mock
	private ProductcategoryService productcategoryService; // Injected by Spring
	@Mock
	private RatingService ratingService; // Injected by Spring
	@Mock
	private WhishlistService whishlistService; // Injected by Spring

	private ProductFactoryForTest productFactoryForTest = new ProductFactoryForTest();
	private MusictypeFactoryForTest musictypeFactoryForTest = new MusictypeFactoryForTest();
	private ArtistFactoryForTest artistFactoryForTest = new ArtistFactoryForTest();
	private ProductcategoryFactoryForTest productcategoryFactoryForTest = new ProductcategoryFactoryForTest();
	private RatingFactoryForTest ratingFactoryForTest = new RatingFactoryForTest();
	private WhishlistFactoryForTest whishlistFactoryForTest = new WhishlistFactoryForTest();

	List<Musictype> musictypes = new ArrayList<Musictype>();
	List<Artist> artists = new ArrayList<Artist>();
	List<Productcategory> productcategorys = new ArrayList<Productcategory>();
	List<Rating> ratings = new ArrayList<Rating>();
	List<Whishlist> whishlists = new ArrayList<Whishlist>();

	private void givenPopulateModel() {
		Musictype musictype1 = musictypeFactoryForTest.newMusictype();
		Musictype musictype2 = musictypeFactoryForTest.newMusictype();
		List<Musictype> musictypes = new ArrayList<Musictype>();
		musictypes.add(musictype1);
		musictypes.add(musictype2);
		when(musictypeService.findAll()).thenReturn(musictypes);

		Artist artist1 = artistFactoryForTest.newArtist();
		Artist artist2 = artistFactoryForTest.newArtist();
		List<Artist> artists = new ArrayList<Artist>();
		artists.add(artist1);
		artists.add(artist2);
		when(artistService.findAll()).thenReturn(artists);

		Productcategory productcategory1 = productcategoryFactoryForTest.newProductcategory();
		Productcategory productcategory2 = productcategoryFactoryForTest.newProductcategory();
		List<Productcategory> productcategorys = new ArrayList<Productcategory>();
		productcategorys.add(productcategory1);
		productcategorys.add(productcategory2);
		when(productcategoryService.findAll()).thenReturn(productcategorys);

		Rating rating1 = ratingFactoryForTest.newRating();
		Rating rating2 = ratingFactoryForTest.newRating();
		List<Rating> ratings = new ArrayList<Rating>();
		ratings.add(rating1);
		ratings.add(rating2);
		when(ratingService.findAll()).thenReturn(ratings);

		Whishlist whishlist1 = whishlistFactoryForTest.newWhishlist();
		Whishlist whishlist2 = whishlistFactoryForTest.newWhishlist();
		List<Whishlist> whishlists = new ArrayList<Whishlist>();
		whishlists.add(whishlist1);
		whishlists.add(whishlist2);
		when(whishlistService.findAll()).thenReturn(whishlists);

	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<Product> list = new ArrayList<Product>();
		when(productService.findAll()).thenReturn(list);
		
		// When
		String viewName = productController.list("", "", "", "", model);
		
		// Then
		assertEquals("product/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = productController.formForCreate(model);
		
		// Then
		assertEquals("product/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		//assertNull(((Product)modelMap.get("product")).getId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/product/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<MusictypeListItem> musictypeListItems = (List<MusictypeListItem>) modelMap.get("listOfMusictypeItems");
		assertEquals(2, musictypeListItems.size());
		
		@SuppressWarnings("unchecked")
		List<ArtistListItem> artistListItems = (List<ArtistListItem>) modelMap.get("listOfArtistItems");
		assertEquals(2, artistListItems.size());
		
		@SuppressWarnings("unchecked")
		List<ProductcategoryListItem> productcategoryListItems = (List<ProductcategoryListItem>) modelMap.get("listOfProductcategoryItems");
		assertEquals(2, productcategoryListItems.size());
		
		@SuppressWarnings("unchecked")
		List<RatingListItem> ratingListItems = (List<RatingListItem>) modelMap.get("listOfRatingItems");
		assertEquals(2, ratingListItems.size());
		
		@SuppressWarnings("unchecked")
		List<WhishlistListItem> whishlistListItems = (List<WhishlistListItem>) modelMap.get("listOfWhishlistItems");
		assertEquals(2, whishlistListItems.size());
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Product product = productFactoryForTest.newProduct();
		Integer id = product.getId();
		when(productService.findById(id)).thenReturn(product);
		
		// When
		String viewName = productController.formForUpdate(model, id);
		
		// Then
		assertEquals("product/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(product, (Product) modelMap.get("product"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/product/update", modelMap.get("saveAction"));
		
		List<ProductcategoryListItem> productcategoryListItems = (List<ProductcategoryListItem>) modelMap.get("listOfProductcategoryItems");
		assertEquals(2, productcategoryListItems.size());
		
		List<ArtistListItem> artistListItems = (List<ArtistListItem>) modelMap.get("listOfArtistItems");
		assertEquals(2, artistListItems.size());
		
		List<MusictypeListItem> musictypeListItems = (List<MusictypeListItem>) modelMap.get("listOfMusictypeItems");
		assertEquals(2, musictypeListItems.size());
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Product product = productFactoryForTest.newProduct();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Product productCreated = new Product();
		when(productService.create(product)).thenReturn(productCreated); 
		
		// When
		String viewName = productController.create(product, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/product/form/"+product.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(productCreated, (Product) modelMap.get("product"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Product product = productFactoryForTest.newProduct();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = productController.create(product, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("product/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(product, (Product) modelMap.get("product"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/product/create", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<MusictypeListItem> musictypeListItems = (List<MusictypeListItem>) modelMap.get("listOfMusictypeItems");
		assertEquals(2, musictypeListItems.size());
		
		@SuppressWarnings("unchecked")
		List<ArtistListItem> artistListItems = (List<ArtistListItem>) modelMap.get("listOfArtistItems");
		assertEquals(2, artistListItems.size());
		
		@SuppressWarnings("unchecked")
		List<ProductcategoryListItem> productcategoryListItems = (List<ProductcategoryListItem>) modelMap.get("listOfProductcategoryItems");
		assertEquals(2, productcategoryListItems.size());
		
		@SuppressWarnings("unchecked")
		List<RatingListItem> ratingListItems = (List<RatingListItem>) modelMap.get("listOfRatingItems");
		assertEquals(2, ratingListItems.size());
		
		@SuppressWarnings("unchecked")
		List<WhishlistListItem> whishlistListItems = (List<WhishlistListItem>) modelMap.get("listOfWhishlistItems");
		assertEquals(2, whishlistListItems.size());
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Product product = productFactoryForTest.newProduct();
		
		Exception exception = new RuntimeException("test exception");
		when(productService.create(product)).thenThrow(exception);
		
		// When
		String viewName = productController.create(product, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("product/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(product, (Product) modelMap.get("product"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/product/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "product.error.create", exception);
		
		@SuppressWarnings("unchecked")
		List<MusictypeListItem> musictypeListItems = (List<MusictypeListItem>) modelMap.get("listOfMusictypeItems");
		assertEquals(2, musictypeListItems.size());
		
		@SuppressWarnings("unchecked")
		List<ArtistListItem> artistListItems = (List<ArtistListItem>) modelMap.get("listOfArtistItems");
		assertEquals(2, artistListItems.size());
		
		@SuppressWarnings("unchecked")
		List<ProductcategoryListItem> productcategoryListItems = (List<ProductcategoryListItem>) modelMap.get("listOfProductcategoryItems");
		assertEquals(2, productcategoryListItems.size());
		
		@SuppressWarnings("unchecked")
		List<RatingListItem> ratingListItems = (List<RatingListItem>) modelMap.get("listOfRatingItems");
		assertEquals(2, ratingListItems.size());
		
		@SuppressWarnings("unchecked")
		List<WhishlistListItem> whishlistListItems = (List<WhishlistListItem>) modelMap.get("listOfWhishlistItems");
		assertEquals(2, whishlistListItems.size());
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		Product product = productFactoryForTest.newProduct();
		Integer id = product.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		Product productSaved = new Product();
		productSaved.setId(id);
		when(productService.update(product)).thenReturn(productSaved); 
		
		// When
		String viewName = productController.update(product, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/product/form/"+product.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(productSaved, (Product) modelMap.get("product"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		Product product = productFactoryForTest.newProduct();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = productController.update(product, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("product/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(product, (Product) modelMap.get("product"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/product/update", modelMap.get("saveAction"));
		
		@SuppressWarnings("unchecked")
		List<ProductcategoryListItem> productcategoryListItems = (List<ProductcategoryListItem>) modelMap.get("listOfProductcategoryItems");
		assertEquals(2, productcategoryListItems.size());
		
		@SuppressWarnings("unchecked")
		List<ArtistListItem> artistListItems = (List<ArtistListItem>) modelMap.get("listOfArtistItems");
		assertEquals(2, artistListItems.size());
		
		@SuppressWarnings("unchecked")
		List<MusictypeListItem> musictypeListItems = (List<MusictypeListItem>) modelMap.get("listOfMusictypeItems");
		assertEquals(2, musictypeListItems.size());
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		Product product = productFactoryForTest.newProduct();
		
		Exception exception = new RuntimeException("test exception");
		when(productService.update(product)).thenThrow(exception);
		
		// When
		String viewName = productController.update(product, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("product/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(product, (Product) modelMap.get("product"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/product/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "product.error.update", exception);
		
		@SuppressWarnings("unchecked")
		List<ProductcategoryListItem> productcategoryListItems = (List<ProductcategoryListItem>) modelMap.get("listOfProductcategoryItems");
		assertEquals(2, productcategoryListItems.size());
		
		@SuppressWarnings("unchecked")
		List<ArtistListItem> artistListItems = (List<ArtistListItem>) modelMap.get("listOfArtistItems");
		assertEquals(2, artistListItems.size());
		
		@SuppressWarnings("unchecked")
		List<MusictypeListItem> musictypeListItems = (List<MusictypeListItem>) modelMap.get("listOfMusictypeItems");
		assertEquals(2, musictypeListItems.size());
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Product product = productFactoryForTest.newProduct();
		Integer id = product.getId();
		
		// When
		String viewName = productController.delete(redirectAttributes, id);
		
		// Then
		verify(productService).delete(id);
		assertEquals("redirect:/product", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		Product product = productFactoryForTest.newProduct();
		Integer id = product.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(productService).delete(id);
		
		// When
		String viewName = productController.delete(redirectAttributes, id);
		
		// Then
		verify(productService).delete(id);
		assertEquals("redirect:/product", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "product.error.delete", exception);
	}
	
	
}
