package com.capsoagritrancommerce.web.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//--- Entities
import com.capsoagritrancommerce.entity.TypePay;
import com.capsoagritrancommerce.test.TypePayFactoryForTest;

//--- Services 
import com.capsoagritrancommerce.business.service.TypePayService;


import com.capsoagritrancommerce.web.common.Message;
import com.capsoagritrancommerce.web.common.MessageHelper;
import com.capsoagritrancommerce.web.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RunWith(MockitoJUnitRunner.class)
public class TypePayControllerTest {
	
	@InjectMocks
	private TypePayController typePayController;
	@Mock
	private TypePayService typePayService;
	@Mock
	private MessageHelper messageHelper;
	@Mock
	private MessageSource messageSource;

	private TypePayFactoryForTest typePayFactoryForTest = new TypePayFactoryForTest();


	private void givenPopulateModel() {
	}

	@Test
	public void list() {
		// Given
		Model model = new ExtendedModelMap();
		
		List<TypePay> list = new ArrayList<TypePay>();
		when(typePayService.findAll()).thenReturn(list);
		
		// When
		String viewName = typePayController.list(model);
		
		// Then
		assertEquals("typePay/list", viewName);
		Map<String,?> modelMap = model.asMap();
		assertEquals(list, modelMap.get("list"));
	}
	
	@Test
	public void formForCreate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		// When
		String viewName = typePayController.formForCreate(model);
		
		// Then
		assertEquals("typePay/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertNull(((TypePay)modelMap.get("typePay")).getId());
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/typePay/create", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void formForUpdate() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		TypePay typePay = typePayFactoryForTest.newTypePay();
		Integer id = typePay.getId();
		when(typePayService.findById(id)).thenReturn(typePay);
		
		// When
		String viewName = typePayController.formForUpdate(model, id);
		
		// Then
		assertEquals("typePay/form", viewName);
		
		Map<String,?> modelMap = model.asMap();
		
		assertEquals(typePay, (TypePay) modelMap.get("typePay"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/typePay/update", modelMap.get("saveAction"));
		
	}
	
	@Test
	public void createOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		TypePay typePay = typePayFactoryForTest.newTypePay();
		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		TypePay typePayCreated = new TypePay();
		when(typePayService.create(typePay)).thenReturn(typePayCreated); 
		
		// When
		String viewName = typePayController.create(typePay, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/typePay/form/"+typePay.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(typePayCreated, (TypePay) modelMap.get("typePay"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void createBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		TypePay typePay = typePayFactoryForTest.newTypePay();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = typePayController.create(typePay, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("typePay/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(typePay, (TypePay) modelMap.get("typePay"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/typePay/create", modelMap.get("saveAction"));
		
	}

	@Test
	public void createException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		TypePay typePay = typePayFactoryForTest.newTypePay();
		
		Exception exception = new RuntimeException("test exception");
		when(typePayService.create(typePay)).thenThrow(exception);
		
		// When
		String viewName = typePayController.create(typePay, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("typePay/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(typePay, (TypePay) modelMap.get("typePay"));
		assertEquals("create", modelMap.get("mode"));
		assertEquals("/typePay/create", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "typePay.error.create", exception);
		
	}

	@Test
	public void updateOk() {
		// Given
		Model model = new ExtendedModelMap();
		
		TypePay typePay = typePayFactoryForTest.newTypePay();
		Integer id = typePay.getId();

		BindingResult bindingResult = mock(BindingResult.class);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		TypePay typePaySaved = new TypePay();
		typePaySaved.setId(id);
		when(typePayService.update(typePay)).thenReturn(typePaySaved); 
		
		// When
		String viewName = typePayController.update(typePay, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("redirect:/typePay/form/"+typePay.getId(), viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(typePaySaved, (TypePay) modelMap.get("typePay"));
		assertEquals(null, modelMap.get("mode"));
		assertEquals(null, modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"save.ok"));
	}

	@Test
	public void updateBindingResultErrors() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		TypePay typePay = typePayFactoryForTest.newTypePay();
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(true);
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		
		// When
		String viewName = typePayController.update(typePay, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("typePay/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(typePay, (TypePay) modelMap.get("typePay"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/typePay/update", modelMap.get("saveAction"));
		
	}

	@Test
	public void updateException() {
		// Given
		Model model = new ExtendedModelMap();
		
		givenPopulateModel();
		
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
		BindingResult bindingResult = mock(BindingResult.class);
		when(bindingResult.hasErrors()).thenReturn(false);

		TypePay typePay = typePayFactoryForTest.newTypePay();
		
		Exception exception = new RuntimeException("test exception");
		when(typePayService.update(typePay)).thenThrow(exception);
		
		// When
		String viewName = typePayController.update(typePay, bindingResult, model, redirectAttributes, httpServletRequest);
		
		// Then
		assertEquals("typePay/form", viewName);

		Map<String,?> modelMap = model.asMap();
		
		assertEquals(typePay, (TypePay) modelMap.get("typePay"));
		assertEquals("update", modelMap.get("mode"));
		assertEquals("/typePay/update", modelMap.get("saveAction"));
		
		Mockito.verify(messageHelper).addException(model, "typePay.error.update", exception);
		
	}
	

	@Test
	public void deleteOK() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		TypePay typePay = typePayFactoryForTest.newTypePay();
		Integer id = typePay.getId();
		
		// When
		String viewName = typePayController.delete(redirectAttributes, id);
		
		// Then
		verify(typePayService).delete(id);
		assertEquals("redirect:/typePay", viewName);
		Mockito.verify(messageHelper).addMessage(redirectAttributes, new Message(MessageType.SUCCESS,"delete.ok"));
	}

	@Test
	public void deleteException() {
		// Given
		RedirectAttributes redirectAttributes = mock(RedirectAttributes.class);
		
		TypePay typePay = typePayFactoryForTest.newTypePay();
		Integer id = typePay.getId();
		
		Exception exception = new RuntimeException("test exception");
		doThrow(exception).when(typePayService).delete(id);
		
		// When
		String viewName = typePayController.delete(redirectAttributes, id);
		
		// Then
		verify(typePayService).delete(id);
		assertEquals("redirect:/typePay", viewName);
		Mockito.verify(messageHelper).addException(redirectAttributes, "typePay.error.delete", exception);
	}
	
	
}
