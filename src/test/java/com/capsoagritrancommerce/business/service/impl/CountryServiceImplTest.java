/*
 * Created on 8 mars 2015 ( Time 12:20:35 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package com.capsoagritrancommerce.business.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.capsoagritrancommerce.entity.Country;
import com.capsoagritrancommerce.entity.jpa.CountryEntity;
import java.util.List;
import com.capsoagritrancommerce.business.service.mapping.CountryServiceMapper;
import com.capsoagritrancommerce.data.repository.jpa.CountryJpaRepository;
import com.capsoagritrancommerce.test.CountryFactoryForTest;
import com.capsoagritrancommerce.test.CountryEntityFactoryForTest;
import com.capsoagritrancommerce.test.MockValues;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Test : Implementation of CountryService
 */
@RunWith(MockitoJUnitRunner.class)
public class CountryServiceImplTest {

	@InjectMocks
	private CountryServiceImpl countryService;
	@Mock
	private CountryJpaRepository countryJpaRepository;
	@Mock
	private CountryServiceMapper countryServiceMapper;
	
	private CountryFactoryForTest countryFactoryForTest = new CountryFactoryForTest();

	private CountryEntityFactoryForTest countryEntityFactoryForTest = new CountryEntityFactoryForTest();

	private MockValues mockValues = new MockValues();
	
	@Test
	public void findById() {
		// Given
		Integer id = mockValues.nextInteger();
		
		CountryEntity countryEntity = countryJpaRepository.findOne(id);
		
		Country country = countryFactoryForTest.newCountry();
		when(countryServiceMapper.mapCountryEntityToCountry(countryEntity)).thenReturn(country);

		// When
		Country countryFound = countryService.findById(id);

		// Then
		assertEquals(country.getId(),countryFound.getId());
	}

	@Test
	public void findAll() {
		// Given
		List<CountryEntity> countryEntitys = new ArrayList<CountryEntity>();
		CountryEntity countryEntity1 = countryEntityFactoryForTest.newCountryEntity();
		countryEntitys.add(countryEntity1);
		CountryEntity countryEntity2 = countryEntityFactoryForTest.newCountryEntity();
		countryEntitys.add(countryEntity2);
		when(countryJpaRepository.findAll()).thenReturn(countryEntitys);
		
		Country country1 = countryFactoryForTest.newCountry();
		when(countryServiceMapper.mapCountryEntityToCountry(countryEntity1)).thenReturn(country1);
		Country country2 = countryFactoryForTest.newCountry();
		when(countryServiceMapper.mapCountryEntityToCountry(countryEntity2)).thenReturn(country2);

		// When
		List<Country> countrysFounds = countryService.findAll();

		// Then
		assertTrue(country1 == countrysFounds.get(0));
		assertTrue(country2 == countrysFounds.get(1));
	}

	@Test
	public void create() {
		// Given
		Country country = countryFactoryForTest.newCountry();

		CountryEntity countryEntity = countryEntityFactoryForTest.newCountryEntity();
		when(countryJpaRepository.findOne(country.getId())).thenReturn(null);
		
		countryEntity = new CountryEntity();
		countryServiceMapper.mapCountryToCountryEntity(country, countryEntity);
		CountryEntity countryEntitySaved = countryJpaRepository.save(countryEntity);
		
		Country countrySaved = countryFactoryForTest.newCountry();
		when(countryServiceMapper.mapCountryEntityToCountry(countryEntitySaved)).thenReturn(countrySaved);

		// When
		Country countryResult = countryService.create(country);

		// Then
		assertTrue(countryResult == countrySaved);
	}

	@Test
	public void createKOExists() {
		// Given
		Country country = countryFactoryForTest.newCountry();

		CountryEntity countryEntity = countryEntityFactoryForTest.newCountryEntity();
		when(countryJpaRepository.findOne(country.getId())).thenReturn(countryEntity);

		// When
		Exception exception = null;
		try {
			countryService.create(country);
		} catch(Exception e) {
			exception = e;
		}

		// Then
		assertTrue(exception instanceof IllegalStateException);
		assertEquals("already.exists", exception.getMessage());
	}

	@Test
	public void update() {
		// Given
		Country country = countryFactoryForTest.newCountry();

		CountryEntity countryEntity = countryEntityFactoryForTest.newCountryEntity();
		when(countryJpaRepository.findOne(country.getId())).thenReturn(countryEntity);
		
		CountryEntity countryEntitySaved = countryEntityFactoryForTest.newCountryEntity();
		when(countryJpaRepository.save(countryEntity)).thenReturn(countryEntitySaved);
		
		Country countrySaved = countryFactoryForTest.newCountry();
		when(countryServiceMapper.mapCountryEntityToCountry(countryEntitySaved)).thenReturn(countrySaved);

		// When
		Country countryResult = countryService.update(country);

		// Then
		verify(countryServiceMapper).mapCountryToCountryEntity(country, countryEntity);
		assertTrue(countryResult == countrySaved);
	}

	@Test
	public void delete() {
		// Given
		Integer id = mockValues.nextInteger();

		// When
		countryService.delete(id);

		// Then
		verify(countryJpaRepository).delete(id);
		
	}

}
