-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Sam 21 Mars 2015 à 11:44
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `designpattern`
--

-- --------------------------------------------------------

--
-- Structure de la table `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `addressname` varchar(45) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `number` varchar(45) NOT NULL,
  `street` varchar(45) DEFAULT NULL,
  `zipcode` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `complement` varchar(45) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_adress_country1_idx` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `artist`
--

CREATE TABLE IF NOT EXISTS `artist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `artist`
--

INSERT INTO `artist` (`id`, `name`) VALUES
(1, 'Jean-Jacques Goldman'),
(2, 'Francis Cabrel'),
(3, 'Michael Jackson'),
(4, 'The Beatles'),
(5, 'Mozart'),
(6, 'Julien Clerc'),
(7, 'Indochine'),
(8, 'Muse'),
(9, 'Eminem'),
(10, 'Queen');

-- --------------------------------------------------------

--
-- Structure de la table `basket`
--

CREATE TABLE IF NOT EXISTS `basket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_basket_user1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `basket_has_product`
--

CREATE TABLE IF NOT EXISTS `basket_has_product` (
  `basket_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`basket_id`,`product_id`),
  KEY `fk_basket_has_product_product1_idx` (`product_id`),
  KEY `fk_basket_has_product_basket1_idx` (`basket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `command`
--

CREATE TABLE IF NOT EXISTS `command` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `total` decimal(2,2) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `type_pay_id` int(11) NOT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_command_user1_idx` (`user_id`),
  KEY `fk_command_type_pay1_idx` (`type_pay_id`),
  KEY `fk_command_discount1_idx` (`discount_id`),
  KEY `fk_command_status1_idx` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `command_has_product`
--

CREATE TABLE IF NOT EXISTS `command_has_product` (
  `command_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `cost` decimal(2,2) DEFAULT NULL,
  PRIMARY KEY (`command_id`,`product_id`),
  KEY `fk_command_has_product_product1_idx` (`product_id`),
  KEY `fk_command_has_product_command1_idx` (`command_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `country`
--

INSERT INTO `country` (`id`, `name`) VALUES
(1, 'France'),
(2, 'Belegique'),
(3, 'Irlande'),
(4, 'Angleterre'),
(5, 'Ecosse'),
(6, 'Suisse');

-- --------------------------------------------------------

--
-- Structure de la table `discount`
--

CREATE TABLE IF NOT EXISTS `discount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL,
  `percentage` int(11) NOT NULL,
  `qte` int(11) DEFAULT NULL,
  `validity` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `musictype`
--

CREATE TABLE IF NOT EXISTS `musictype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `musictype`
--

INSERT INTO `musictype` (`id`, `name`) VALUES
(1, 'Rock'),
(2, 'Rap'),
(3, 'Techno'),
(4, 'Jazz'),
(5, 'Electronique'),
(6, 'Classique'),
(7, 'Variété francaise'),
(8, 'Variété étrangère'),
(9, 'Pop'),
(10, 'Blues'),
(11, 'Métal'),
(12, 'Funk'),
(13, 'Punk');

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `image` varchar(45) DEFAULT NULL,
  `description` text NOT NULL,
  `release_year` int(11) DEFAULT NULL,
  `cost` decimal(10,2) NOT NULL,
  `demat` tinyint(1) DEFAULT NULL,
  `ref` varchar(45) NOT NULL,
  `qte` int(11) NOT NULL,
  `productCategory_id` int(11) NOT NULL,
  `artist_id` int(11) DEFAULT NULL,
  `musicType_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_productCategory1_idx` (`productCategory_id`),
  KEY `fk_product_artist1_idx` (`artist_id`),
  KEY `fk_product_musicType1_idx` (`musicType_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `product`
--

INSERT INTO `product` (`id`, `name`, `image`, `description`, `release_year`, `cost`, `demat`, `ref`, `qte`, `productCategory_id`, `artist_id`, `musicType_id`) VALUES
(2, 'Muse : The Resistance', 'product2.jpg', 'L''ALBUM INDISPENSABLE ! LA DEFERLANTE MUSE CONTINUE <br><br>\r\nUn nouvel album de Muse est toujours un événement. Depuis le miraculeux Showbiz, premier effort des Anglais paru en 1999, Muse n’a cessé d’essayer de maintenir la barre, avec des albums plus ou moins convaincants. Qu’en est-il de ce cinquième opus du groupe de Matthew Bellamy ? « Uprising », titre d’ouverture et single, propose une mélodie tirée par les cheveux où se croisent un clin d’œil évident au « Fade to Grey » de Visage, et le sempiternel organe incantatoire (quasi religieux) de Bellamy. Peu convaincant. L’enchaînement sur « Resistance » rassure sur son refrain, urgent, tendu et entêtant. Puis le décalé « Undisclosed Desires » ouvre une autre voie : celle des années 1980, ou Depeche Mode n’est jamais bien loin.', 2010, '10.00', 0, 'fffqfee78ffeg', 3, 3, 8, 1),
(3, 'Eminem : The Slim Shady LP', 'product3.jpg', 'La découverte rap de 1999 : un Visage pâle virtuose accro à l''humour noir et aux images choc. White trash hip-hop de Detroit, le dénommé Marshall Mathers (M & M, d''où le pseudo) avait sorti un premier album en 1996, Infinite, passé inaperçu. Puis il fut repéré par un homme au flair sûr, Dr. Dre, et signé sur son label Aftermath. Roi de la provoc'' et des textes sans concession, Eminem/Slim Shady y va franco : "J''en n''ai rien à battre, Dieu m''a amené ici pour foutre le boxon. (...) Tu m''étonnes que mon cerveau soit niqué, je suis né pendant un tremblement de terre !" Le garnement ne respecte strictement rien ni personne (excepté sa fille Hailie) : drogue, violence, dérives psychologiques en tout genre sont pour lui autant de sujets de plaisanterie, au même titre que le sida. Il n''épargne pas moins ses propres parents et la mère de sa fille, également brocardés sans pitié. Mais c''est surtout son flow preste, mis en valeur par la production sobre de Dr. Dre sur trois titres, qui met tout le monde d''accord.', 2013, '8.00', 0, 'ezffez854fez', 0, 3, 9, 2),
(4, 'Goldman : Chansons pour les pieds', 'product4.jpg', '« L''unique ambition de ces chansons : faire que des gens se lèvent, se regardent, se parlent, se frôlent, chantent et dansent. Juste des chansons pour les pieds... » Voici ce que livre Jean-Jacques Goldman sur son nouvel album. Une sorte de pied de nez à une certaine idée de la chanson française, qui doit automatiquement être une chanson à textes... Mais ce n''est pas pour autant que Jean-Jacques Goldman n''a pas soigné ses paroles, dont les thèmes demeurent inchangés : un regard doux-amer sur la société et l''amour.\r\n<br></br>\r\nC''est donc bel et bien côté musique que Jean-Jacques Goldman étonne dans ce nouvel opus. Même si sa patte est bien présente sur de traditionnels rocks, ballades (La Pluie...) et hymnes rassembleurs (Ensemble), le compositeur a forcé le trait de certaines de ces influences au niveau des arrangements : celtique, disco et même techno (l''étonnant C''est pas vrai). Un melting-pot qui offre ce qui semble être à ce jour le meilleur album de Goldman, en tout cas le plus étonnant.', 2002, '7.00', 0, 'vdfv515ezf', 50, 3, 1, 7);

-- --------------------------------------------------------

--
-- Structure de la table `productcategory`
--

CREATE TABLE IF NOT EXISTS `productcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `image` varchar(45) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `productcategory`
--

INSERT INTO `productcategory` (`id`, `name`, `image`, `description`) VALUES
(1, 'DVD', '', ''),
(2, 'Blu-ray', '', ''),
(3, 'CD', '', ''),
(4, 'Vinyle', '', ''),
(5, 'VOD', '', ''),
(6, 'Musique dématérialisée', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `product_has_rating`
--

CREATE TABLE IF NOT EXISTS `product_has_rating` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `rating_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`rating_id`),
  KEY `fk_product_has_rating_rating1_idx` (`rating_id`),
  KEY `fk_product_has_rating_product1_idx` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `product_has_rating`
--

INSERT INTO `product_has_rating` (`product_id`, `rating_id`) VALUES
(2, 1),
(2, 2),
(2, 3),
(2, 4);

-- --------------------------------------------------------

--
-- Structure de la table `rating`
--

CREATE TABLE IF NOT EXISTS `rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mark` int(11) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `rating`
--

INSERT INTO `rating` (`id`, `mark`, `comment`) VALUES
(1, 4, 'Très bon CD.'),
(2, 5, 'Muse toujours aussi excellent !'),
(3, 4, 'Pas le meilleur album de Muse, mais reste très bon.'),
(4, 5, 'J''adoooooooore !');

-- --------------------------------------------------------

--
-- Structure de la table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `status`
--

INSERT INTO `status` (`id`, `nom`) VALUES
(1, 'En attente de payement'),
(2, 'Payée'),
(3, 'Préparation en cours'),
(4, 'Livrée'),
(5, 'Terminée'),
(6, 'Annulée');

-- --------------------------------------------------------

--
-- Structure de la table `type_pay`
--

CREATE TABLE IF NOT EXISTS `type_pay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `type_pay`
--

INSERT INTO `type_pay` (`id`, `name`) VALUES
(1, 'CB'),
(2, 'Cheque'),
(3, 'Virement');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `password` varchar(500) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `newsletter` tinyint(1) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `address` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_country1_idx` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `password`, `phone`, `newsletter`, `country_id`, `username`, `enabled`, `address`) VALUES
(1, 'Thomas', 'Cichocki', 'password', '0676797411', NULL, 1, 'th.cichocki@gmail.com', 1, '7C rue de Boulogne 59800 Lille'),
(2, 'Jean', 'Dupont', 'password', '0676797411', NULL, 1, 'user@user.com', 1, '7C rue de Boulogne 59800 Lille'),
(28, 'gezgzegze', 'gezgzzze', 'password', '0320304050', NULL, 1, 'test@test.com', 1, '7 rue de Paris 62000 Arras'),
(29, 'bgrber', 'breberb', '$2a$10$oXYBLBHADgtH3AlA.dUs3./4AuhGGg10i58ba4lKQAzEO1wOd4QEC', 'eebzbezzeb', NULL, 2, 'gzg@bezbez.com', 1, 'ebzzebzeb'),
(30, 'fgzgzeg', 'gzgzzeg', '$2a$10$UBefb8rK995xM8k1ZUa9eO2Apq5a83yAW7hOIM2b3Ak6X4D5l.2VW', 'gzagza', NULL, 1, 'gg@gg.com', 1, 'gzagazgza'),
(31, 'hnrtnr', 'nrtnrt', '$2a$10$U0bVsTTayM2bheeTMQtjYeb7PQzpDNC6YqsEeuwB9KGlf5d80JdTK', 'gfezgze', NULL, 3, 'b@b.com', 1, 'gezgze'),
(32, 'gsds', 'bdsbdsdsb', '$2a$10$g0dmQQw0yek0Uo.AMxN0Vezrbki93He9kImjgia1a2D38hcQ1.R7O', '0320304050', NULL, 1, 'ffffzjhjh@fdsfd.com', 1, 'vezvzezve');

-- --------------------------------------------------------

--
-- Structure de la table `user_has_address`
--

CREATE TABLE IF NOT EXISTS `user_has_address` (
  `user_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`address_id`),
  KEY `fk_user_has_adress_adress1_idx` (`address_id`),
  KEY `fk_user_has_adress_user1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `ROLE` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_username_role` (`ROLE`,`username`),
  KEY `fk_username_idx` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `user_roles`
--

INSERT INTO `user_roles` (`id`, `username`, `ROLE`) VALUES
(2, 'th.cichocki@gmail.com', 'ROLE_ADMIN'),
(13, 'b@b.com', 'ROLE_USER'),
(14, 'ffffzjhjh@fdsfd.com', 'ROLE_USER'),
(12, 'gg@gg.com', 'ROLE_USER'),
(11, 'gzg@bezbez.com', 'ROLE_USER'),
(10, 'test@test.com', 'ROLE_USER'),
(1, 'th.cichocki@gmail.com', 'ROLE_USER'),
(5, 'user@user.com', 'ROLE_USER');

-- --------------------------------------------------------

--
-- Structure de la table `whishlist`
--

CREATE TABLE IF NOT EXISTS `whishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_whishlist_user1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `whishlist`
--

INSERT INTO `whishlist` (`id`, `name`, `date`, `user_id`) VALUES
(1, 'Cadeaux noel', '2015-03-21 11:35:54', 1),
(2, 'I love it', '2015-03-17 00:00:00', 1);

-- --------------------------------------------------------

--
-- Structure de la table `whishlist_has_product`
--

CREATE TABLE IF NOT EXISTS `whishlist_has_product` (
  `whishlist_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`whishlist_id`,`product_id`),
  KEY `fk_whishlist_has_product_product1_idx` (`product_id`),
  KEY `fk_whishlist_has_product_whishlist1_idx` (`whishlist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `whishlist_has_product`
--

INSERT INTO `whishlist_has_product` (`whishlist_id`, `product_id`) VALUES
(1, 2);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `fk_adress_country1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `basket`
--
ALTER TABLE `basket`
  ADD CONSTRAINT `fk_basket_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `basket_has_product`
--
ALTER TABLE `basket_has_product`
  ADD CONSTRAINT `fk_basket_has_product_basket1` FOREIGN KEY (`basket_id`) REFERENCES `basket` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_basket_has_product_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `command`
--
ALTER TABLE `command`
  ADD CONSTRAINT `fk_command_discount1` FOREIGN KEY (`discount_id`) REFERENCES `discount` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_command_status1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_command_type_pay1` FOREIGN KEY (`type_pay_id`) REFERENCES `type_pay` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_command_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `command_has_product`
--
ALTER TABLE `command_has_product`
  ADD CONSTRAINT `fk_command_has_product_command1` FOREIGN KEY (`command_id`) REFERENCES `command` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_command_has_product_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_product_artist1` FOREIGN KEY (`artist_id`) REFERENCES `artist` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_musicType1` FOREIGN KEY (`musicType_id`) REFERENCES `musictype` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_productCategory1` FOREIGN KEY (`productCategory_id`) REFERENCES `productcategory` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `product_has_rating`
--
ALTER TABLE `product_has_rating`
  ADD CONSTRAINT `fk_product_has_rating_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_has_rating_rating1` FOREIGN KEY (`rating_id`) REFERENCES `rating` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_country1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `user_has_address`
--
ALTER TABLE `user_has_address`
  ADD CONSTRAINT `fk_user_has_adress_adress1` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_has_adress_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `whishlist`
--
ALTER TABLE `whishlist`
  ADD CONSTRAINT `fk_whishlist_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `whishlist_has_product`
--
ALTER TABLE `whishlist_has_product`
  ADD CONSTRAINT `fk_whishlist_has_product_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_whishlist_has_product_whishlist1` FOREIGN KEY (`whishlist_id`) REFERENCES `whishlist` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
